export LANG=en_US.UTF-8
export LC_TIME=en_US.UTF-8

# Shell
export TERM=xterm-256color
export SHELL="$(which zsh)"

# Use nvim as man pager
export MANPAGER="nvim --cmd 'let g:unception_disable=1' +Man!"
# export MANPAGER="man_page"

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi
export VISUAL=$EDITOR
export SUDO_EDITOR=$EDITOR

##################
# local settings #
##################
export TERM_EMULATOR=kitty
export EMAIL_CLIENT="$TERM_EMULATOR -e aerc"
export MESSAGE_CLIENT="$TERM_EMULATOR -e scli"
export WM_CHOICES="sway (wayland)\nherbstluftwm (X)\nawesome (X)"

# Don't use default venv prompt
export VIRTUAL_ENV_DISABLE_PROMPT=1

########
# rust #
########
export PATH=$PATH:~/.cargo/bin

#######
# nix #
#######
# export PATH=$PATH:~/.nix-profile/bin

##########
# golang #
##########
GOBIN=/usr/local/go/bin
GOLIBBIN=~/dev/golib/bin
GOCODEBIN=~/dev/go/bin
GOPATHLIB=~/dev/golib
GOPATHCODE=~/dev/go
export PATH="$PATH:$GOBIN:$GOLIBBIN:$GOCODEBIN"
export GOPATH="$GOPATHLIB:$GOPATHCODE"

#######
# npm #
#######
export PATH="$PATH:"~/.local/share/npm/bin

############
# clipmenu #
############
export CM_LAUNCHER=rofi
export CM_IGNORE_WINDOW=KeePass
export CM_SELECTIONS=clipboard

##############
# keyringman #
##############
export KM_KEYRING_NAME='Default_5fkeyring'

#######
# fzf #
#######
export FZF_DEFAULT_COMMAND='fd --hidden --type f --exclude .git'
# tokyonight
export FZF_DEFAULT_OPTS=' 
--color=fg:#c0caf5,bg:#1a1b26,hl:#bb9af7
--color=fg+:#c0caf5,bg+:#1a1b26,hl+:#7dcfff
--color=info:#7aa2f7,prompt:#7dcfff,pointer:#7dcfff 
--color=marker:#9ece6a,spinner:#9ece6a,header:#9ece6a'

#######
# xdg #
#######
export XDG_SCREENSHOTS_DIR=~/pictures/screenshots

########
# node #
########
export N_PREFIX=~/.local/share/n

#############
# verilator #
#############
export PYMTL_VERILATOR_INCLUDE_DIR=~/.nix-profile/share/verilator/include
