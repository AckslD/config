_fzf_complete_manven() {
  _fzf_complete --reverse --prompt="venv> " -- "$@" < <(
    manven list
  )
}

_fzf_complete_smanven() {
    _fzf_complete_manven
}
