###########
# general #
###########
function grepft() {
    grep -rn --include=\*.$1 $2
}

#######
# fzf #
#######
function frep() {
    grep $@ | fzf
}

function frepft() {
    grepft $@ | fzf
}

function vimfzf() {
    for arg in $@; do
        case $arg in
            -o|--open-last)
                local LAST=1
                ;;
        esac
    done
    if [[ $LAST -eq 1 && -n $VIMFZF_LAST_FILE ]]; then
        nv "$VIMFZF_LAST_FILE"
        return
    fi
    local file=$(fzf_files)
    if [[ -n $file ]]; then
        VIMFZF_LAST_FILE=$file
        nv "$file"
    fi
}

function livegrep() {
    INITIAL_QUERY=""
    RG_PREFIX="rg --hidden --column --line-number --no-heading --color=always --smart-case "
    (FZF_DEFAULT_COMMAND="$RG_PREFIX '$INITIAL_QUERY'" fzf --bind "change:reload:$RG_PREFIX {q} || true" --ansi --disabled --query "$INITIAL_QUERY" --height=50% --layout=reverse --delimiter=':' --preview="bat {1} --line-range {2}: --highlight-line {2}")
}

########
# todo #
########

function vimtodo() {
    if [[ -z $TODO_ROOT_FOLDER ]]; then
        TODO_DIR=~/todo
    else
        TODO_DIR=$TODO_ROOT_FOLDER
    fi
    TODO_PATH=$TODO_DIR/todo.txt
    setopt +o nomatch
    CONFLICT_FILE=$(ls $TODO_DIR/todo*conflict* 2> /dev/null)
    setopt -o nomatch
    if [[ -z $CONFLICT_FILE ]]; then
        vim $TODO_PATH
    else
        echo "vim -d $CONFLICT_FILE $TODO_PATH"
        vim -d $CONFLICT_FILE $TODO_PATH
        read "yn?Delete the conflict file $CONFLICT_FILE? (y/n)"
        if [[ "$yn" == "y" ]]; then
            rm $CONFLICT_FILE
        fi
    fi
}

#########
# latex #
#########
function pdflatexfull() {
    [[ $# == 1 ]] || echo "No file specified" && return 1
    pdflatex $1
    bibtex ${1:0:-4}
    pdflatex $1
    pdflatex $1
}

#######
# git #
#######
# Checkout a PR locally
function gcpr() {
    ID=$1
    BRANCH_NAME=$2
    git fetch origin "pull/$ID/head:$BRANCH_NAME"
    git checkout $BRANCH_NAME
}

######
# lf #
######
# Change working dir in shell to last dir in lf on exit (adapted from ranger).
function lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
                cd "$dir"
            fi
        fi
    fi
}

########
# vifm #
########
function vicd() {
    local dst="$(command vifmrun --choose-dir - "$@")"
    if [ -z "$dst" ]; then
        return 0
    fi
    cd "$dst"
}

################
# Note manager #
################
function noteman() {
    local _flag_help
    local _flag_list
    # Parse args
    for arg in $@; do
        case $arg in
            -h|--help)
                _flag_help="set"
                ;;
            -l|--list)
                _flag_list="set"
                ;;
            -o|--open)
                _flag_open="set"
                ;;
        esac
    done

    # Check help
    if [[ "$_flag_help" != "" ]];then
        echo "A note manager, simply use without arguments."
        echo "To list current notes, use -l/--list."
        return
    fi

    if [[ -z $TODO_ROOT_FOLDER ]]; then
        TODO_DIR=~/todo
    else
        TODO_DIR=$TODO_ROOT_FOLDER
    fi
    NOTES_FOLDER=$TODO_DIR/notes

    # Check list notes
    if [[ "$_flag_list" != "" ]];then
        find "$NOTES_FOLDER" -type f
        return
    fi

    # Check list notes
    if [[ "$_flag_open" != "" ]];then
        vim "$(noteman -l | fzf)"
        return
    fi

    # Create the scratch folder if it does not exist
    TMP_FILE=$NOTES_FOLDER/.scratch.md
    LAST_TMP_FILE=$NOTES_FOLDER/.last_scratch.md
    LOCK_FILE=$NOTES_FOLDER/.lock
    mkdir -p "$NOTES_FOLDER"

    # Lock the scratch file in order to not overwrite a current one
    if [[ -f $LOCK_FILE ]]; then
      echo "Scratch file already in use, if not remove $LOCK_FILE"
      return 1
    fi
    touch "$LOCK_FILE"

    # Backup last scratch file
    if [[ -f $TMP_FILE ]]; then
      cp "$TMP_FILE" "$LAST_TMP_FILE"
    fi

    # Reset the scratch file
    rm -f "$TMP_FILE"
    touch "$TMP_FILE"

    # Open vim to edit
    vim "$TMP_FILE"

    # Remove lock
    rm "$LOCK_FILE"

    # Ask for a name (use date and time by default)
    BASEFILENAME=$(date +"%Y_%m_%d")
    DEFAULT_FILENAME="$(date +"%H%M%S").md"
    read -r "?Give your notes a name default: $DEFAULT_FILENAME: " notes_name
    if [[ "$notes_name" = "" ]];then
        TARGET_FILE=$NOTES_FOLDER/${BASEFILENAME}_$DEFAULT_FILENAME
    else
        TARGET_FILE=$NOTES_FOLDER/${BASEFILENAME}_$notes_name
    fi

    # Add default extension if not given
    HAS_EXT=$(echo "$TARGET_FILE" | grep '\.')
    if [[ -z "$HAS_EXT" ]]; then
        TARGET_FILE=$TARGET_FILE.md
    fi

    # Copy/save the temporary scratch file
    cp "$TMP_FILE" "$TARGET_FILE"

    echo "File saved at $TARGET_FILE"
}

#############
# clipboard #
#############
function clipdelall() {
    CLIPMENUDIR=$(find $XDG_RUNTIME_DIR -type d -name "clipmenu*" 2> /dev/null)
    for filepath in $CLIPMENUDIR/*; do
        filename=$(basename -- "$filepath")
        for keep in lock session_lock status; do
            if [[ $filename == $keep ]]; then
                ignore="yes"
            fi
        done
        if [[ -z $ignore ]]; then
            rm $filepath
        fi
    done
}

##################
# Lock and sleep #
##################
function lockAndSleep() {
    for arg in $@; do
        case $arg in
            -s|--sleep)
                SLEEP="true"
                ;;
        esac
    done
    keyringman lock
    if [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
        swaylock && [[ "$SLEEP" == "true" ]] && systemctl suspend
    else
        if hash i3lock; then
            i3lock -i ~/pictures/background.png -t && [[ "$SLEEP" == "true" ]] && systemctl suspend
        elif hash gnome-screensaver; then
            [[ -z "$(gnome-screensaver-command -q)" ]] && gnome-screensaver &
            gnome-screensaver-command -l
            if [[ "$SLEEP" == "true" ]]; then
                sleep 1
                systemctl suspend
            fi
        fi
    fi

}

##########
# pacman #
##########
function pprune() {
    sudo pacman -Rns $(pacman -Qtdq)
    sudo paccache -r
    sudo paccache -ruk0
}

#############
# bluetooth #
#############
function connect_bt_device() {
    if ! systemctl status bluetooth.service > /dev/null; then
        systemctl start bluetooth.service
    fi
    bluetoothctl power on
    bluetoothctl connect $@
}

#############
# alacritty #
#############
function alacritty-theme() {
    if ! [[ -f ~/.config/alacritty/color.yml ]]; then
      echo "file ~/.config/alacritty/color.yml doesn't exist"
      return
    fi

    # sed doesn't like symlinks, get the absolute path
    config_path=$(realpath ~/.config/alacritty/color.yml)

    theme=$@
    sed -i -e "s#^colors: \*.*#colors: *$theme#g" $config_path

    echo "switched to $theme in $config_path"

    export ALACRITTY_THEME=$theme
    echo "ALACRITTY_THEME=\"$ALACRITTY_THEME\"" > ~/.config/zsh/setup/alacritty_theme.sh
}

function alacritty-theme-toggle() {
    if [[ $ALACRITTY_THEME == "gruvbox_light" ]]; then
        alacritty-theme gruvbox_dark
    else
        alacritty-theme gruvbox_light
    fi
}


##########
# ffmpeg #
##########
function make_gif() {
    for file in $@; do
        ffmpeg -i "$file" -vf "fps=8,scale=1000:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop -1 "$file".gif
    done
}

##############
# expressvpn #
##############
function ev() {
    local location=$(expressvpn list all | tail -n +3 | fzf | cut -d" " -f1)
    if [[ -n $location ]]; then
        expressvpn connect $location
    fi
}


#######
# pip #
#######
function pipadd() {
    CURRENT=$(mktemp)
    DEPS=$(mktemp)
    TEMP_TARGET=$(mktemp -d)
    FILTERED_DEPS=$(mktemp)
    function cleanup() {
        rm "$CURRENT"
        rm "$DEPS"
        rm "$FILTERED_DEPS"
        rm -r "$TEMP_TARGET"
    }
    trap cleanup EXIT

    # Store current packages in a file
    pip list --format freeze | sed -e 's/\(\w*\)==.*/\1/' > $CURRENT
    # Install current directory to temporary target and extract dependencies
    # Note that this will exclude the current package itself
    pip -e . -t $TEMP_TARGET
    (PYTHONPATH=$TEMP_TARGET pip freeze --exclude-editable > $DEPS)
    # Extract install_requires from setup.py to a requirements file
    # rg --multiline --multiline-dotall 'install_requires=\[[^]]*\]' setup.py | tail -n +2 | head -n -1 | sed -e "s/\s*'\(.*\)',/\1/" > $DEPS
    # Filter out what's already installed
    rg -v -f $CURRENT $DEPS > $FILTERED_DEPS
    echo "Installing with dependencies:"
    cat $FILTERED_DEPS
    pip -e . --no-deps -r $FILTERED_DEPS
}

##########
# python #
##########
function p() {
  if [[ -n $VIRTUAL_ENV ]]; then
    python $@
  else
    if [[ -f pyrightconfig.json ]]; then
      venv_python=$(jq --raw-output '"\(.["venvPath"])\(.["venv"])"' pyrightconfig.json)
      "$venv_python"/bin/python $@
    else
      python $@
    fi
  fi
}

function dpy() {
  pid=$(ps a | fzf | awk '{print $1}')
  echo "attaching to $pid"
  python -m debugpy --listen localhost:5678 --pid $pid
}

function gprof() {
  gprof2dot -f pstats "$1" | dot -Tpng -o "$1.png"
}

###########
# jupyter #
###########
function jpc() {
  for file in $@; do
    jupyter nbconvert --clear-output "$file"
    jq --indent 1 'del(.metadata.jupytext)' "$file" > "$file.tmp" && mv "$file.tmp" "$file"
  done
}
