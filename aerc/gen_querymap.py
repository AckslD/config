import os
import sys
from runpy import run_path
from subprocess import check_output

PATH_TO_HERE = os.path.abspath(os.path.dirname(__file__))
SYNC_NOTMUCH_MAILDIR_SCRIPT = check_output(
    'which sync-notmuch-maildir',
    shell=True,
).decode(sys.getdefaultencoding()).strip()

tag_folders = run_path(SYNC_NOTMUCH_MAILDIR_SCRIPT)['tag_folders']


def create_querymap():
    folders = tag_folders()
    querymap = {"All Mail": "*"}

    for tag, folder in folders.items():
        exclude = ' '.join([f'tag:{t}' for t in folders if t != tag] + ['tag:deleted'])
        querymap[folder.replace('Folders.', '')] = f"(folder:{folder} OR tag:{tag}) AND NOT ({exclude})"

    return querymap


def save_querymap(querymap):
    querymap_path = os.path.join(PATH_TO_HERE, "querymap")
    with open(querymap_path, 'w') as f:
        f.writelines([f"{key}={value}\n" for key, value in querymap.items()])


def main():
    querymap = create_querymap()
    save_querymap(querymap)


if __name__ == '__main__':
    main()
