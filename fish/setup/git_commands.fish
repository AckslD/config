#############
# Git stuff #
#############
# Github
alias gs "git status"
alias gd "git diff"
alias gp "git pull"
alias gl "git log --all --oneline --graph"

# Git prune remote
alias gpr "git remote update origin --prune"

# Git finish will push to current branch
# Eg. gf "commit message" or gf -a "commit message"
function gf
    set CURRENT_BRANCH (git rev-parse --abbrev-ref HEAD)
    git commit -m "$argv[1]" && git push origin "$CURRENT_BRANCH"
end

# Git merge
# Eg. gm branch-name
function gm
    git merge "$argv[1]"
end

# Git checkout
# Eg. gc branch-name
function gc
    git checkout "$argv[1]" && gp
end

# Git remove branch
function gr
    git branch -d "$argv[1]"
end
function grr
    git branch -D "$argv[1]"
end

# When finished with a branch, clean remote and remove branch
function gdone
    set CURRENT_BRANCH (git rev-parse --abbrev-ref HEAD)
    gc master && gpr && gr "$CURRENT_BRANCH"
end

# Checkout a PR locally
function gcpr
    set ID "$argv[1]"
    set BRANCH_NAME "$argv[2]"
    git fetch origin "pull/$ID/head:$BRANCH_NAME"
    git checkout $BRANCH_NAME
end
#############
