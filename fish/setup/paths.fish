# JetBrains
set PYCHARM ~/bin/pycharm_2019/bin
set CLION ~/bin/clion_2019/bin
set -xg PATH $PATH $PYCHARM $CLION

# Local (needed for eg flake8)
set LOCALBIN ~/.local/bin
set -xg PATH $PATH $LOCALBIN

# pyenv
set -x PYENV_ROOT ~/.pyenv
set -x PATH "$PYENV_ROOT/bin" $PATH
