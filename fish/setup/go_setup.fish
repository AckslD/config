# GO
set GOBIN /usr/local/go/bin
set GOLIBBIN ~/dev/golib/bin
set GOCODEBIN ~/dev/go/bin
set GOPATHLIB ~/dev/golib
set GOPATHCODE $GOPATH ~/dev/go
set -xg PATH $PATH $GOBIN $GOLIBBIN $GOCODEBIN
set -xg GOPATH $GOPATHLIB:$GOPATHCODE
