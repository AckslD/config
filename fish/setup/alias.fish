if test (which xdg-open)
    alias open xdg-open
end

alias cflags "set -xg CFLAGS '-mmacosx-version-min=10.7 -std=c++11 -stdlib=libc++'"
alias uncflags "set -xg CFLAGS ''"

alias vim nvim

if test -d ~/.pyenv
    alias smanven "source (pyenv which manven.fish)"
else
    alias smanven "source (which manven.fish)"
end
