set -xg LC_ALL en_US.UTF-8
set -xg LANG en_US.UTF-8

# VI mode
fish_vi_key_bindings

# zen for oh-my-fish
function my-zen-init --on-event zen.init
	zen tmux new-window vim
end

# Editor
set -xg EDITOR nvim

# Disable default virtualenv prompt (to use the agnoster one instead)
set -gx VIRTUAL_ENV_DISABLE_PROMPT 1

# Netsquid pip
function nspip
    if test -z "$NETSQUIDPYPI_USER"
        echo "Please set NETSQUIDPYPI_USER first"
        return
        exit 1
    end
    if test -z "$NETSQUIDPYPI_PWD"
        echo "Please set NETSQUIDPYPI_PWD first"
        return
        exit 1
    end
    pip3 install --extra-index-url=https://$NETSQUIDPYPI_USER:$NETSQUIDPYPI_PWD@pypi.netsquid.org $argv
end

# vim -> xclip
function vimxclip
    set vimxclip_path ~/.local/share/vimxclip
    set vimxclip_file $vimxclip_path/content
    mkdir -p $vimxclip_path
    rm -f $vimxclip_file
    vim $vimxclip_file
    truncate -s -1 $vimxclip_file
    # cat $vimxclip_file | xclip -selection clipboard
    cat $vimxclip_file | xsel --input --clipboard
end

set path_to_here (dirname (status --current-filename))
for file in $path_to_here/setup/*
    source $file
end

if test (which pyenv)
    source (pyenv init - | psub)
end

# Lock and sleep
function lockAndSleep
    gnome-screensaver-command -l
    sleep 1
    systemctl suspend
end
