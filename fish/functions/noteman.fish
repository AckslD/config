function noteman
    # Parse arguments
    argparse 'h/help' 'l/list' -- $argv

    # Check help
    if test "$_flag_help" != ""
        echo "A note manager, simply use without arguments."
        echo "To list current notes, use -l/--list."
        return
    end

    set SCRATCH_FOLDER ~/documents/notes/scratch

    # Check list notes
    if test "$_flag_list" != ""
        find $SCRATCH_FOLDER -type f
        return
    end

    # Create the scratch folder if it does not exist
    set TMP_FILE $SCRATCH_FOLDER/.scratch
    mkdir -p $SCRATCH_FOLDER

    # Reset the scratch file
    rm -f $TMP_FILE
    touch $TMP_FILE

    # Open vim to edit
    vim $TMP_FILE

    # Ask for a name (use date and time by default)
    set DEFAULT_FOLDER (date +"%Y/%m/%d")
    set DEFAULT_FILENAME (date +"%H%M%S")
    read -P "Give your notes a name default: $DEFAULT_FILENAME: " notes_name
    if test "$notes_name" = ""
        set TARGET_FILE $SCRATCH_FOLDER/$DEFAULT_FOLDER/$DEFAULT_FILENAME
    else
        set TARGET_FILE $SCRATCH_FOLDER/$DEFAULT_FOLDER/$notes_name
    end

    # Create the folder if it does not exist
    mkdir -p $SCRATCH_FOLDER/$DEFAULT_FOLDER

    # Copy/save the temporary scratch file
    cp $TMP_FILE $TARGET_FILE

    echo "File saved at $TARGET_FILE"
end
