#!/usr/bin/env bash
for arg in "$@"; do
    case $arg in
        -f|--fast)
            fast=1
            ;;
        -q|--quiet)
            quiet=1
            ;;
    esac
done

# Function for making symbolic links from ~/.file to ~/.config/app/file
function symblink() {
    config_file="$1"
    config_folder="$2"
    if [[ $quiet -eq 1 ]]; then
        echo "Replacing ~/.${config_file} with a symbolic link to ~/.config/${config_folder}/${config_file}."
    else
        echo "This will replace ~/.${config_file} with a symbolic link to ~/.config/${config_folder}/${config_file}, do you want to continue?"
        read -r -n 1 -p "Press enter to continue..."
    fi

    rm -f "$HOME/.${config_file}"
    ln -s "$HOME/.config/${config_folder}/${config_file}" "$HOME/.${config_file}"
}

# Copies all bash scripts in bin-folders to ~/.local/bin and makes them executable
function copy_all_bin() {
    echo "Copying executables to ~/.local/bin"
    mkdir -p ~/.local/bin
    for bin_folder in $(find . -type d \( -name "bin" ! -wholename "*/arch/*" \)); do
        for bin in $bin_folder/*; do
            binname=$(basename -- "$bin")
            echo -e "\t$bin -> ~/.local/bin/$binname"
            cp $bin ~/.local/bin
            chmod +x ~/.local/bin/$binname
        done
    done
}

# Copies all qutebrowser userscripts in to ~/.local/share/qutebrowser/userscripts and makes them executable
function copy_qute_userscripts() {
    source=~/.config/qutebrowser/userscripts
    target=~/.local/share/qutebrowser/userscripts
    echo "Copying qutebrowser userscripts to $target"
    mkdir -p $target
    cp $source/* $target
    chmod +x $target/*
}

if [[ $quiet -eq 1 ]]; then
    echo "Overwriting files in ~/.config."
else
    echo "This will overwrite files in ~/.config, do you want to continue?"
    read -r -n 1 -p "Press enter to continue..."
fi

# Make dir if it does not exist
mkdir -p ~/.config

# needs to be removed in order to not replace the symbolic link
rm -f ~/.config/herbstluftwm/panel.sh

# copy over files
cp -r ./* ~/.config
rm -f ~/.config/install.sh

# Use barpyrus for herbstluftwm
echo "Creating symbolic link ~/.config/herbstluftwm/panel.sh -> ~/dev/barpyrus/barpyrus.py"
# ln -sf ~/dev/barpyrus/barpyrus.py ~/.config/herbstluftwm/panel.sh
# echo "Creating symbolic link ~/.config/herbstluftwm/panel.sh -> ~/.config/polybar/start.sh"
# ln -s ~/.config/polybar/start.sh ~/.config/herbstluftwm/panel.sh

# alacritty
rm -f ~/.config/zsh/setup/alacritty_theme.sh

# Updated data in aerc/accounts.conf
echo "Updating aerc/accounts.conf variables"
replace-vars ~/.config/aerc/accounts.conf MYEMAIL IMAP_PWD MYNAME
# correct @ signs
sed -i '/\(source\|outgoing\)/s/@/%40/' ~/.config/aerc/accounts.conf
# echo "Creating querymap for aerc"
# python ~/.config/aerc/gen_querymap.py
# rm ~/.config/aerc/gen_querymap.py

# Create a symbolic link to the tmux config
symblink tmux.conf tmux

# Create a symbolic link to the flake8 config
symblink flake8 flake8

symblink isort.cfg isort

# Create a symbolic link to the git config
symblink gitconfig git
symblink gitignore git
echo "Updating gitconfig variables"
replace-vars ~/.config/git/gitconfig MYNAME MYGITEMAIL

# Create a symbolic link to the xprofile config
symblink xinitrc x
symblink xprofile x
symblink Xresources x
symblink xserverrc x

# Create a symbolic link to the zsh config
symblink zshrc zsh
symblink zshenv zsh
symblink zprofile zsh

# Create a symbolic link to the xbindkeys config
symblink xbindkeysrc xbindkeys

symblink gtkrc-2.0 gtk-2.0

symblink profile profile

symblink czrc commitizen

# Email
symblink offlineimaprc offlineimap
symblink notmuch-config notmuch
echo "Updating offlineimaprc variables"
replace-vars ~/.config/offlineimap/offlineimaprc MYEMAIL IMAP_PWD
echo "Updating notmuch-config variables"
replace-vars ~/.config/notmuch/notmuch-config MYNAME MYEMAIL
echo "Updating neomuttrc variables"
replace-vars ~/.config/neomutt/neomuttrc MYEMAIL IMAP_PWD
echo "Updating himalaya variables"
replace-vars ~/.config/himalaya/config.toml MYNAME MYEMAIL IMAP_PWD

# Contacts and calendar
replace-vars ~/.config/vdirsyncer/config ETESYNC_USER ETESYNC_PWD

# jupyter
symblink jupyter/custom/custom.css jupyter

copy_all_bin
copy_qute_userscripts
# TODO only if exists
chmod +x ~/.config/redshift/hooks/backlight.sh

# Install language servers
if [[ $fast -eq 1 ]]; then
    echo "Will skip slow installs"
else
    if [[ $quiet -eq 1 ]]; then
        yn="y"
    else
        read -r -p "To install languages servers enter y, to skip enter something else...(y/n)" yn
    fi
    if [ "$yn" == "y" ]; then
        echo "Installing languages servers for nvim"
        # TODO check these
        sudo npm install -g pyright # python
        rustup component add rust-src # rust
        sudo npm install -g vim-language-server # vimscript
        sudo npm install -g bash-language-server # bash
        sudo npm install -g dockerfile-language-server-nodejs # docker
        sudo npm install -g vscode-json-languageserver # json
        cargo install --git https://github.com/latex-lsp/texlab.git --locked # tex
        sudo npm install -g yaml-language-server # yaml
        sudo npm install -g vscode-html-languageserver-bin # html
        sudo pacman -S ccls # c/c++
        yay -S lua-language-server # lua
        sudo pacman -S efm-langserver # linting
    fi
fi
