# make sure zsh is installed

# bash ~/nix-install.sh --daemon

echo "add nixpkgs channel"
nix-channel --add https://nixos.org/channels/nixpkgs-unstable
nix-channel --update

echo "add home-manager channel"
nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager
nix-channel --update

echo "install home manager"
nix-shell '<home-manager>' -A install
