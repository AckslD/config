{ config, pkgs, lib, ... }:

with lib;
let
  # get the basename from a path
  basename = path: lists.last (strings.splitString "/" (toString path));
  # recursively list files in a path and put them in a set using value of `make_name` as the key
  # opts are merged with {source = file} as the value
  # TODO how to exclude .git stuff?
  # TODO for eg zsh, kitty how to include git (submodules)
  make_files =
    { dir
    , make_name ? file: (path.removePrefix dir file)
    , opts ? { }
    }: builtins.listToAttrs (forEach (filesystem.listFilesRecursive dir) (file: {
      name = (make_name file);
      value = {
        source = file;
      } // opts;
    }));
  # create dot-files to symlink based on the files in `./dotfiles`
  # TODO how to symlink barpyrus?
  dot_files = make_files { dir = ./dotfiles; };
  # create bin-files (executable) to symlink based on the files in `./bin`
  bin_files = make_files {
    dir = ./bin;
    make_name = file: ".local/bin/" + (basename file);
    opts = { executable = true; };
  };
  # nixGL = import <nixgl> {};
in
{
  nixpkgs.config.allowUnfree = true;

  home.username = "axel";
  home.homeDirectory = "/home/axel";
  home.stateVersion = "23.11";

  home.packages = [
    pkgs.neovim
    pkgs.eza
    pkgs.ripgrep
    pkgs.lazygit
    pkgs.fzf
    pkgs.fd
    pkgs.zoxide
    pkgs.bat
    pkgs.dust
    pkgs.tealdeer
    # TODO currently broken, building from source for now
    # pkgs.haskellPackages.kmonad
    pkgs.rclone
    pkgs.jq
    pkgs.tree-sitter
    # TODO not yet working, part of neovim config?
    # pkgs.nixd

    pkgs.pyright
    pkgs.basedpyright
    pkgs.bash-language-server
    pkgs.vscode-langservers-extracted
    pkgs.python312Packages.jedi-language-server
    pkgs.yaml-language-server
    pkgs.shellcheck
    pkgs.lua-language-server

    # (pkgs.python3.withPackages(ps: [ps.virtualenv]))
    # pkgs.python311Packages.pip
    # pkgs.pythonPackages.manven

    pkgs.vifm
    pkgs.ueberzug
    pkgs.ffmpegthumbnailer
    pkgs.imagemagick
    pkgs.poppler
    pkgs.djvulibre
    pkgs.gnome-epub-thumbnailer
    pkgs.fontpreview

    pkgs.batsignal
    # pkgs.conky
    # pkgs.lemonbar
    # pkgs.clipmenu
    pkgs.rofi
    # pkgs.sway
    pkgs.waybar
    pkgs.feh
    pkgs.lemonbar-xft
    pkgs.autorandr
    pkgs.gscreenshot
    pkgs.zathura
    pkgs.sxiv
    pkgs.kitty
    pkgs.qutebrowser
    pkgs.uv
    # pkgs.picom
    # TODO how to do this?
    # nixGL.auto.nixGLNvidia

    # pkgs.slack
    # pkgs.firefox
    # pkgs.drawio
    # pkgs.keepassxc

    pkgs.verilator
  ];

  # home.file = dot_files // bin_files;

  programs.home-manager.enable = true;

  services.clipmenu.enable = true;

  systemd.user.services.kmonad = {
    Unit = {
      Description = "kmonad keyboard config";
    };
    Install = {
      WantedBy=["default.target"];
    };
    Service = {
      Restart="always";
      RestartSec=3;
      # ExecStart="/usr/bin/kmonad /home/axel/.config/kmonad/config.kbd";
      # ExecStart="${pkgs.writeShellScript "start-kmonad" "kmonad ~/.config/kmonad/config.kbd"}";
      # ExecStart="/nix/store/933wi1pcyldrwnmh8jgb0fh9cgmr1msd-kmonad-0.4.2/bin/kmonad /home/axel/.config/kmonad/config.kbd";
      ExecStart="/home/axel/.local/bin/kmonad /home/axel/.config/kmonad/config.kbd";
      Nice=-20;
    };
  };
}
