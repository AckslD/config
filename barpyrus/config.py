import os
import sys

from barpyrus import hlwm
from barpyrus import widgets as W
from barpyrus.core import Theme
from barpyrus import lemonbar
from barpyrus import conky
from barpyrus.colors import (
    AQUA_LIGHT,
    GREEN_LIGHT,
    GREEN_DARK,
    YELLOW_LIGHT,
    PURPLE_LIGHT,
    BLUE_LIGHT,
    ORANGE_LIGHT,
    RED_LIGHT,
    GRAY_LIGHT,
    FG,
    BG,
)
from barpyrus.conky import col_fmt
# Copy this config to ~/.config/barpyrus/config.py

# What icons to use
from enum import Enum, auto


class IconSet(Enum):
    SIJI = auto()
    AWESOME = auto()


# icon_set = IconSet.SIJI
icon_set = IconSet.AWESOME

# Conky config
update_interval = '1'
conky_config = {
    'update_interval': update_interval,
}

# set up a connection to herbstluftwm in order to get events
# and in order to call herbstclient commands
hc = hlwm.connect()

# get the geometry of the monitor
monitor = sys.argv[1] if len(sys.argv) >= 2 else 0
(x, y, monitor_w, monitor_h) = hc.monitor_rect(monitor)
height = 14  # height of the panel
width = monitor_w  # width of the panel
hc(['pad', str(monitor), str(height)])  # get space for the panel

# common elements
empty_label = W.RawLabel('')


# Conky setup
def custom():
    return ''


def mail():
    mail_symb = {
        IconSet.SIJI: '\ue1a8',
        IconSet.AWESOME: '\uf0e0',
    }[icon_set]
    inbox = os.path.expanduser('~/.mail/INBOX')
    return col_fmt(AQUA_LIGHT) + mail_symb + col_fmt(FG) + ' ${unseen_mails %s}/${mails %s}' % (inbox, inbox)


def disk_usage():
    disk_symb = {
        IconSet.SIJI: '\ue1bb',
        IconSet.AWESOME: '\uf0c7',
    }[icon_set]
    return col_fmt(GREEN_LIGHT) + disk_symb + col_fmt(FG) + ' ${fs_free}/${fs_size}'


def uptime():
    on_symb = {
        IconSet.SIJI: '\ue10c',
        IconSet.AWESOME: '\uf011',
    }[icon_set]
    return col_fmt(GREEN_LIGHT) + on_symb + col_fmt(FG) + ' ${uptime_short}'


##########
# volume #
##########
def volume():
    vol_off_symb = col_fmt(FG) + {
        IconSet.SIJI: '\ue202',
        IconSet.AWESOME: '\uf6a9',
    }[icon_set]
    speaker_symb = {
        IconSet.SIJI: '\ue203',
        IconSet.AWESOME: '\uf028',
    }[icon_set]
    headphone_symb = {
        IconSet.SIJI: '\ue0fd',
        IconSet.AWESOME: '\uf025',
    }[icon_set]
    vol_on_symb = '${if_match \"$pa_sink_active_port_name\" == \"analog-output-headphones\"}'
    vol_on_symb += col_fmt(YELLOW_LIGHT) + headphone_symb
    vol_on_symb += '${else}'

    vol_on_symb += '${if_match \"$pa_sink_active_port_name\" == \"headset-output\"}'
    vol_on_symb += col_fmt(BLUE_LIGHT) + headphone_symb
    vol_on_symb += '${else}'
    vol_on_symb += col_fmt(YELLOW_LIGHT) + speaker_symb
    vol_on_symb += '${endif}'

    vol_on_symb += '${endif}'

    volume = '${if_pa_sink_muted}' + vol_off_symb + '${else}' + vol_on_symb + '${endif}'
    volume += col_fmt(FG) + ' ${pa_sink_volume}%'
    return volume


########
# wifi #
########
# Find the wifi name
wifi_name = None
for net in os.listdir('/sys/class/net'):
    if net.startswith('wl'):
        wifi_name = net
        break


def wifi():
    no_wifi_symb = {
        IconSet.SIJI: '\ue217',
        IconSet.AWESOME: '\uf00d',
    }[icon_set]
    wifi_icons = {
        IconSet.SIJI: ['\ue218', '\ue219', '\ue21a'],
        IconSet.AWESOME: ['\uf1eb'],
    }[icon_set]
    wifi_delta = 100 / len(wifi_icons)
    # for tabbed wifi name
    wifi_col = col_fmt(BLUE_LIGHT)
    if wifi_name is None:
        wifi = col_fmt(FG) + no_wifi_symb
    else:
        wifi = '${if_up %s}' % wifi_name
        wifi_qual = '${wireless_link_qual_perc %s}' % wifi_name
        for i, icon in enumerate(wifi_icons[:-1]):
            wifi += '${if_match ' + wifi_qual + ' < %d}' % ((i + 1) * wifi_delta)
            wifi += wifi_col + icon
            wifi += '${else}'
        wifi += wifi_col + wifi_icons[-1]  # 100 %
        for _ in range(len(wifi_icons) - 1):
            wifi += '${endif}'
        wifi += col_fmt(FG) + ' ' + wifi_qual + '% '
        wifi += '${else}'
        wifi += col_fmt(FG) + no_wifi_symb
        wifi += ' off'
        wifi += '${endif}'
    return wifi


###########
# various #
###########
def cpu():
    cpu_symb = {
        IconSet.SIJI: '\ue026',
        IconSet.AWESOME: '\uf2db',
    }[icon_set]
    return col_fmt(PURPLE_LIGHT) + cpu_symb + col_fmt(FG) + ' ${cpu}%'


def ram():
    ram_symb = {
        IconSet.SIJI: '\ue021',
        IconSet.AWESOME: '\uf538',
    }[icon_set]
    return col_fmt(PURPLE_LIGHT) + ram_symb + col_fmt(FG) + ' ${memperc}%'


def net_down_speed():
    net_down_speed_symb = {
        IconSet.SIJI: '\ue13c',
        IconSet.AWESOME: '\uf063',
    }[icon_set]
    return col_fmt(RED_LIGHT) + net_down_speed_symb + col_fmt(FG) + ' ${downspeed %s}' % wifi_name


def net_up_speed():
    net_up_speed_symb = {
        IconSet.SIJI: '\ue13b',
        IconSet.AWESOME: '\uf062',
    }[icon_set]
    return col_fmt(GREEN_LIGHT) + net_up_speed_symb + col_fmt(FG) + ' ${upspeed %s}' % wifi_name


###########
# battery #
###########
def battery():
    battery = "${if_existing /sys/class/power_supply/BAT0}"
    # first icon: 0 percent
    # last icon: 100 percent
    bat_icons = {
        IconSet.SIJI: [
            0xe242,
            0xe243,
            0xe244,
            0xe245,
            0xe246,
            0xe247,
            0xe248,
            0xe249,
            0xe24a,
            0xe24b,
        ],
        IconSet.AWESOME: [
            0xf244,
            0xf244,
            0xf243,
            0xf243,
            0xf242,
            0xf242,
            0xf241,
            0xf241,
            0xf240,
            0xf240,
        ],
    }[icon_set]
    charge_icon = {
        IconSet.SIJI: 0xe09e,
        IconSet.AWESOME: 0xf0e7,
    }[icon_set]
    bat_cols = [
        # RED_DARK,
        RED_LIGHT,
        # ORANGE_DARK,
        ORANGE_LIGHT,
        # YELLOW_DARK,
        YELLOW_LIGHT,
    ]
    bat_cols += [GREEN_DARK] * (len(bat_icons) - len(bat_cols))
    bat_delta = 100 / len(bat_icons)

    def _get_bat_col(icon_index):
        bat_col = ''
        bat_col += "${if_match \"$battery\" == \"discharging $battery_percent%\"}"
        bat_col += col_fmt(bat_cols[icon_index])
        bat_col += "$else"
        bat_col += col_fmt(GREEN_LIGHT) + chr(charge_icon) + ' '
        bat_col += "$endif"
        return bat_col

    for i, icon in enumerate(bat_icons[:-1]):
        battery += "${if_match $battery_percent < %d}" % ((i+1)*bat_delta)
        battery += _get_bat_col(i)
        battery += chr(icon)
        battery += "${else}"
    battery += _get_bat_col(-1)
    battery += chr(bat_icons[-1])  # icon for 100 percent
    for _ in bat_icons[:-1]:
        battery += "${endif}"
    battery += " $battery_percent%"
    battery += "${endif}"
    return battery


############
# pomodoro #
############
def pomodoro():
    pomo_focus_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf816',
    }[icon_set]
    # pomo_break_icon = '\uf816'
    pomodoro = '${if_match \"${exec pomodoro-cli status}\" == \"work\"}'
    pomodoro += col_fmt(RED_LIGHT) + pomo_focus_symbol + col_fmt(FG) + ' ${exec pomodoro-cli status remaining}'
    pomodoro += '${else}'
    pomodoro += '${if_match \"${exec pomodoro-cli status}\" == \"inactive\"}'
    pomodoro += col_fmt(GRAY_LIGHT) + pomo_focus_symbol
    pomodoro += '${else}'
    pomodoro += col_fmt(GREEN_LIGHT) + pomo_focus_symbol + col_fmt(FG) + ' ${exec pomodoro-cli status remaining}'
    pomodoro += '${endif}'
    pomodoro += '${endif}'
    return pomodoro


#########
# music #
#########
def music_widget():
    play_symb = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf04b',
    }[icon_set]
    pause_symb = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf04c',
    }[icon_set]
    stop_symb = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf04d',
    }[icon_set]

    query_song_cmd = 'pgrep spt > /dev/null && spt pb -s -f \"%a - %t\" 2> /dev/null'
    music_song = ' ${exec %s}' % query_song_cmd
    query_music_cmd = 'pgrep spt > /dev/null && spt pb -s -f \"%s\"'
    music_symb = '${if_match \"${exec %s}\" == \"Playing\"}' % query_music_cmd
    music_symb += col_fmt(GREEN_LIGHT) + play_symb
    music_symb += '${else}'
    music_symb += '${if_match \"${exec %s}\" == \"Paused\"}' % query_music_cmd
    music_symb += col_fmt(YELLOW_LIGHT) + pause_symb
    music_symb += '${endif}'
    music_symb += '${else}'
    music_symb += col_fmt(GRAY_LIGHT) + stop_symb
    music_symb += '${endif}'

    music = col_fmt(GREEN_LIGHT) + music_symb + col_fmt(FG)

    music_label = conky.ConkyWidget(text=music, config=conky_config)
    music_toggle_cmd = "pgrep spt > /dev/null && spt pb -t"
    music_button = W.ExButton(conky.ConkyWidget(text=music_song, config=conky_config), music_toggle_cmd)

    return W.TabbedLayout([
        (music_label, empty_label),
        (music_label, music_button),
    ])


def wifi_button():
    wifi_text = wifi()
    return W.TabbedLayout([
        (conky.ConkyWidget(text=wifi_text, config=conky_config), empty_label),
        (conky.ConkyWidget(text=wifi_text + ' ${wireless_essid %s}' % wifi_name, config=conky_config), empty_label),
    ])


plain_conky = [
    custom(),
    pomodoro(),
    net_down_speed(),
    net_up_speed(),
    mail(),
    volume(),
    disk_usage(),
    cpu(),
    ram(),
    battery(),
    uptime(),
]
buttons_conky = [wifi_button()]

space_label = W.RawLabel(' ')
conky_widgets = [conky.ConkyWidget(text=' '.join(plain_conky), config=conky_config)]
conky_widgets += buttons_conky
sep_conky_widgets = []
for conky_widget in conky_widgets:
    sep_conky_widgets.append(conky_widget)
    sep_conky_widgets.append(space_label)
conky_list = W.ListLayout(sep_conky_widgets[:-1])


# switch keyboard layout
xkblayouts = [
    'us us us'.split(' '),
    'se se se'.split(' '),
]
setxkbmap = 'setxkbmap'

grey_frame = Theme(bg=BG, fg=FG, padding=(3, 3))

# Widget configuration:
# font = '-*-fixed-medium-*-*--15-*-*-*-*-iso8859-*'
# font = 'Inconsolata-dz for Powerline:Bold:size=12'
font = 'Fira Code:Medium:size=12'
symbol_font = {
    IconSet.SIJI: '-wuncon-siji-medium-r-normal-*-10-100-75-75-c-80-iso10646-1',
    IconSet.AWESOME: 'Font Awesome 6 Free Solid:size=11',
}[icon_set]
bar = lemonbar.Lemonbar(
    geometry=(x, y, width, height),
    font=font,
    symbol_font=symbol_font,
    symbol_vert_offset=2,
)


def get_toggle_app_button(app, symbol, on_color=GREEN_LIGHT, off_color=None):
    return get_toggle_button(
        query_cmd='toggle-app -q %s' % app,
        toggle_cmd='toggle-app %s' % app,
        symbol=symbol,
        on_color=on_color,
        off_color=off_color,
    )


def get_command_button(cmd, symbol, color):
    label = W.ColorLabel(symbol, color)
    return W.ExButton(label, cmd)


def get_toggle_button(
    query_cmd,
    toggle_cmd,
    symbol,
    off_symbol=None,
    on_label='on',
    on_color=GREEN_LIGHT,
    off_color=None,
):
    if off_color is None and off_symbol is None:
        label = W.ColorLabel(symbol, on_color)
    else:
        if off_color is None:
            off_color = on_color
        if off_symbol is None:
            off_symbol = symbol
        text = '${if_match \"${exec %s}\" == \"%s\"}' % (query_cmd, on_label)
        text += col_fmt(on_color) + symbol
        text += '${else}'
        text += col_fmt(off_color) + off_symbol
        text += '${endif}'
        label = conky.ConkyWidget(text=text, config=conky_config)
    return W.ExButton(label, toggle_cmd)


def lightsleeper_button():
    light_on_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf185',
    }[icon_set]
    return get_toggle_app_button(
        app='lightsleeper',
        symbol=light_on_symbol,
        on_color=AQUA_LIGHT,
        off_color=GRAY_LIGHT,
    )


def keyboard_button():
    keyboard_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf11c',
    }[icon_set]
    return get_toggle_app_button(
        app='onboard',
        symbol=keyboard_symbol,
        on_color=AQUA_LIGHT,
        off_color=GRAY_LIGHT,
    )


def finger_touch_button():
    touch_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf577',
    }[icon_set]
    return get_toggle_button(
        query_cmd='toggle-finger -q',
        toggle_cmd='toggle-finger',
        symbol=touch_symbol,
        on_color=AQUA_LIGHT,
        off_color=GRAY_LIGHT,
    )


def dunst_button():
    dunst_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf4ad',
    }[icon_set]
    return get_toggle_button(
        query_cmd='dunstctl is-paused',
        toggle_cmd='dunstctl set-paused toggle',
        on_label='false',
        symbol=dunst_symbol,
        on_color=GREEN_LIGHT,
        off_color=RED_LIGHT,
    )


def pcloud_button():
    sync_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf021',
    }[icon_set]
    return get_toggle_button(
        query_cmd='toggle-app -q pcloud',
        toggle_cmd='gtk-launch pcloud',
        symbol=sync_symbol,
        on_color=GREEN_LIGHT,
        off_color=RED_LIGHT,
    )


def lock_keyring_button():
    lock_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf023',
    }[icon_set]
    unlock_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf3c1',
    }[icon_set]
    return get_toggle_button(
        query_cmd='keyringman status',
        toggle_cmd='keyringman toggle',
        symbol=unlock_symbol,
        off_symbol=lock_symbol,
        on_label='unlocked',
        on_color=RED_LIGHT,
        off_color=GREEN_LIGHT,
    )


def clipman_button():
    clipboard_symbol = {
        IconSet.SIJI: 'TODO',
        IconSet.AWESOME: '\uf0ea',
    }[icon_set]
    return get_toggle_button(
        query_cmd='clipctl status',
        toggle_cmd='clipctl toggle',
        symbol=clipboard_symbol,
        on_label='enabled',
        on_color=AQUA_LIGHT,
        off_color=GRAY_LIGHT,
    )


left_buttons = [
    hlwm.HLWMTags(hc, monitor),
    # Lightsleeper (screen on)
    lightsleeper_button(),
    # Onscreen keyboard
    keyboard_button(),
    # Toggle finger touch
    finger_touch_button(),
    # Dunst (notifications)
    dunst_button(),
    # Toggle pcloud
    pcloud_button(),
    # Lock keyring
    lock_keyring_button(),
    # Clipboard status
    clipman_button(),
    # Mail client status
    # mail_button,
    # Music
    # music_widget(),
]
sep_left_buttons = []
for b in left_buttons:
    sep_left_buttons.append(b)
    sep_left_buttons.append(space_label)
sep_left_buttons = sep_left_buttons[:-1]

window_symbol = {
    IconSet.SIJI: 'TODO',
    IconSet.AWESOME: '\uf2d0',
}[icon_set]

bar.widget = W.ListLayout([
    W.RawLabel('%{l}'),
    *sep_left_buttons,
    W.RawLabel(' ' * 8),
    W.ColorLabel(window_symbol, BLUE_LIGHT),
    space_label,
    grey_frame(hlwm.HLWMWindowTitle(hc)),
    W.RawLabel('%{r}'),
    conky_list,
    W.ShortLongLayout(
        empty_label,
        W.ListLayout([
            hlwm.HLWMLayoutSwitcher(hc, xkblayouts, command=setxkbmap.split(' ')),
            space_label,
        ]),
    ),
    grey_frame(W.DateTime('%a %d %b, %H:%M')),
])
