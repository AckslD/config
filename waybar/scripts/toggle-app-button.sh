#!/usr/bin/env bash
APP="$1"
QUERY_CMD="toggle-app -q $APP"
if [[ -z "$2" ]]; then
    ON_LABEL="on"
else
    ON_LABEL="$2"
fi
~/.config/waybar/scripts/toggle-button.sh "$QUERY_CMD" "$APP" "$ON_LABEL"
