#!/usr/bin/env bash
class=$(pomodoro-cli status)
if [[ "$class" == "work" ]]; then
    text="$(pomodoro-cli status remaining)"
elif [[ "$class" == "inactive" ]]; then
    text=""
else
    text="$(pomodoro-cli status remaining)"
fi
echo "{\"text\": \"$text\", \"alt\": \"$class\", \"tooltip\": \"pomodoro ($class)\", \"class\": \"$class\"}"
