#!/usr/bin/env bash
QUERY_CMD="$1"
if [[ -z "$3" ]]; then
    ON_LABEL="on"
else
    ON_LABEL="$3"
fi
if [[ "$(eval "$QUERY_CMD")" == "$ON_LABEL" ]]; then
    class='on'
else
    class='off'
fi
if [[ -z "$2" ]]; then
    tooltip=""
else
    tooltip="toggle '$2' $class"
fi
echo "{\"alt\": \"$class\", \"tooltip\": \"$tooltip\", \"class\": \"$class\"}"
