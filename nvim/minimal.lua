local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.opt.spell = true

require("lazy").setup({
  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    config = function()
      require('nvim-treesitter.configs').setup({
        ensure_installed = {'python'},
        highlight = {
            enable = true,
            additional_vim_regex_highlighting = true,
        },
      })
    end,
  },
  'EdenEast/nightfox.nvim',
}, {
  root = '/tmp/lazy',
  lockfile = '/tmp/lazy-lock.json',
})

vim.cmd(string.format("colorscheme %s", 'terafox'))
