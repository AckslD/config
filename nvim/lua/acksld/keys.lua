local brj = R('funcs.bracket_jump')

local function set_keymap(mode, opts, keymaps)
  for _, keymap in ipairs(keymaps) do
    vim.keymap.set(mode, keymap[1], keymap[2], vim.tbl_extend('force', opts, keymap[3] or {}))
  end
end

local function clear()
  vim.cmd('nohlsearch')
  vim.lsp.buf.clear_references()
  IfHas('hlslens.main', function(hlslens) hlslens.stop() end)
  IfHas('notify', function(notify) notify.dismiss() end)
end

vim.keymap.set('n', '<Esc>', clear, {desc = 'clear'})
vim.keymap.set('n', '<Left>', brj.prev, {desc = 'prev ([)'})
vim.keymap.set('n', '<Right>', brj.next, {desc = 'next (])'})

-- normal {{{1
set_keymap('n', {noremap=true, silent=true}, {
  -- execute q macro
  {'Q', '@q'},

  -- yank to end of line
  {'Y', 'y$'},

  -- yank/paste clipboard
  {'gy', '"+y'},
  {'gp', '"+p'},
  {'gP', '"+P'},
  {'gY', '"+y$'},

  -- reselect pasted text
  {'gV', '`[v`]'},

  -- source config
  {'<C-s>', ':lua R("funcs.config").source()<CR>'},

  -- Jump list
  {'[j', '<C-o>'},
  {']j', '<C-i>'},

  -- Smart way to move between windows
  {'<C-h>', '<C-w>h'},
  {'<C-j>', '<C-w>j'},
  {'<C-k>', '<C-w>k'},
  {'<C-l>', '<C-w>l'},

  -- Page down/up
  {'[d', '<PageUp>'},
  {']d', '<PageDown>'},

  -- Smart way to move between tabs
  {'<A-h>', 'gT'},
  {'<A-l>', 'gt'},

  -- Resize split
  {'<S-Up>', ':resize -2<CR>'},
  {'<S-Down>', ':resize +2<CR>'},
  {'<S-Left>', ':vertical resize -2<CR>'},
  {'<S-Right>', ':vertical resize +2<CR>'},

  -- Quickfix
  {'<Up>', ':copen<CR>'},
  {'<Down>', ':cclose<CR>'},
  {'[q', ':cprevious<CR>'},
  {']q', ':cnext<CR>'},

  -- Navigate buffers
  {']b', '<Cmd>bnext<CR>'},
  {'[b', '<Cmd>bprev<CR>'},
  {']n', '<Cmd>next<CR>'},
  {'[n', '<Cmd>prev<CR>'},

  -- jump diagnostic
  {'[g', "<Cmd>lua vim.diagnostic.goto_prev({float = true})<CR>"},
  {']g', "<Cmd>lua vim.diagnostic.goto_next({float = true})<CR>"},

  -- fix spelling with first suggestion
  {'gz', 'z=1<CR><CR>'},

  {'cg*', '*Ncgn'},
  {'g.', [[/\V<C-r>"<CR>cgn<C-a><Esc>]]},

  -- center search result
  {'n', 'nzzzv'},
  {'N', 'Nzzzv'},

  -- vifm
  {'<C-n>', function() R('funcs.vifm').select_and_edit() end, {desc = 'vifm'}},

  -- indentation
  {'g=', function()
    R('funcs.keep').cursor_wrap(function()
      print('indenting')
      vim.cmd.normal('gg=G')
    end)()
  end, {desc = 'fix indentation'}},
})

-- visual {{{1
set_keymap('x', {noremap=true, silent=true}, {
  -- Allow pasting same thing many times
  {'p', '""p:let @"=@0<CR>'},

  -- better indent
  {'<', '<gv'},
  {'>', '>gv'},

  -- yank/paste clipboard
  {'gy', '"+y'},
  {'gp', '"+p'},

  -- Visual mode pressing * or # searches for the current selection
  -- {'*', ':<C-u>lua R("funcs.search").visual_selection("/")<CR>/<C-r>=@/<CR><CR>'},
  -- {'#', ':<C-u>lua R("funcs.search").visual_selection("?")<CR>?<C-r>=@/<CR><CR>'},

  -- move selected line(s)
  {'K', ':move \'<-2<CR>gv-gv'},
  {'J', ':move \'>+1<CR>gv-gv'},

  -- repeat dot
  {'.', '<Cmd>normal .<CR>'},
  -- repeat macro
  {'@', '<Cmb>normal Q<CR>'},
})

-- insert {{{1
set_keymap('i', {noremap=true, silent=true}, {
  -- Smart way to move between tabs
  {'<A-h>', [[<C-\><C-n>gT]]},
  {'<A-l>', [[<C-\><C-n>gt]]},

  -- alternative esc
  {'jk', '<Esc>'},

  -- insert special carachters
  {'<C-b>', '<C-k>'},

  -- navigate display lines
  {'<Down>', '<Esc>gja'},
  {'<Up>', '<Esc>gka'},
})
-- set_keymap('i', {noremap=true, silent=true, expr=true}, {
--     -- Completion
--     {'<Tab>', [[pumvisible() ? "\<C-n>" : "\<C-x>\<C-o>"]]},
-- })

-- terminal {{{1
set_keymap('t', {noremap=true, silent=true}, {
  -- paste
  {'<C-v>', [[<C-\><C-n>pi]]},
  -- quickfix from buffer
  {'<C-q>', [[<C-\><C-n>:lua R("funcs.quicklist").create_from_buffer()<CR>]]},

  -- escape in terminal
  {'<Esc>', [[<C-\><C-n>]]},
  {[[<M-\>]], '<Esc>'},

  -- Smart way to move between windows
  {'<C-h>', [[<C-\><C-N><C-w>h]]},
  {'<C-j>', [[<C-\><C-N><C-w>j]]},
  {'<C-k>', [[<C-\><C-N><C-w>k]]},
  {'<C-l>', [[<C-\><C-N><C-w>l]]},

  -- Smart way to move between tabs
  {'<A-h>', [[<C-\><C-N>gT]]},
  {'<A-l>', [[<C-\><C-N>gt]]},
})

local status, wk = pcall(require, "which-key")
if not (status) then
  vim.notify("couldn't load which-key, skipping mappings")
  return
end

-- normal {{{1
wk.add({
  -- general
  {'<leader>w', '<Cmd>w<CR>', desc='save file'},
  {'<leader>W', '<Cmd>wa<CR>', desc = 'save all'},
  {'<leader>x', '<Cmd>q<CR>', desc = 'quit file'},
  {'<leader>X', '<Cmd>qa<CR>', desc = 'quit all'},
  {'<leader>Q', '<Cmd>wa<CR><Cmd>qa<CR>', desc = 'save quit all'},
  {
    '<leader>S',
    string.format('<Cmd>wa<CR><Cmd>mksession! %s/Session.vim<CR><Cmd>qa<CR>', vim.fn.getcwd()),
    desc = 'save session quit all',
  },
  {'<leader><CR>', ':noh<CR>', desc = 'no search hl'},
  {'<leader>*', '*<Cmd>lua R("funcs.search").vim("*."..vim.fn.expand("%:e"))<CR>', desc = 'vimgrep cursor'},

  -- buffers
  {'<leader>b', group = 'buffer'},
  {'<leader>bd', ':Bclose<CR>:tabclose<CR>gT', desc = 'close'},
  {'<leader>ba', ':bufdo bd<CR>', desc = 'do'},
  {'<leader>by', '<Cmd>let @+=expand("%:p")<CR>', desc = 'yank path'},

  -- tabs
  {'<leader>t', group = 'tabs'},
  {'<leader>tn', ':tabnew<CR>', desc = 'new'},
  {'<leader>tt', ':tabnew %<CR><C-o>', desc = 'new current'},
  {'<leader>tN', '<Cmd>lua R("funcs.temp").new()<CR>', desc = 'new temp'},
  {'<leader>to', ':tabonly<CR>', desc = 'close others'},
  {'<leader>tc', ':tabclose<CR>', desc = 'close'},
  {'<leader>tm', ':tabmove ', desc = 'move'},
  {'<leader>te', ':tabedit <C-r>=expand("%:p:h")<CR>/', desc = 'edit cwd'},
  {'<leader>ts', '<Cmd>mksession!<CR>', desc = 'save session'},
  {'<leader>tl', '<Cmd>source Session.vim<CR>', desc = 'load session'},
  {'<leader>tp', function()
    vim.ui.select(vim.api.nvim_list_tabpages(), {
      prompt = 'Select tab',
      format_item = function(item)
        local tab = R('funcs.tabs').get_tab(item)
        return string.format('%s: %s (%s)', item, tab.name, tab.cwd)
      end,
    }, function(choice)
      if choice == nil then
        return
      end
      vim.api.nvim_set_current_tabpage(choice)
    end)
  end, desc = 'pick tab'},

  -- quickfix
  {'<leader>q', group = 'quickfix'},
  {'<leader>qq', ':lua R("funcs.quicklist").create_from_buffer()<CR>', desc = 'from buffer'},
  {'<leader>qe', ':lua R("funcs.quicklist").estream_from_buffer()<CR>', desc = 'from buffer'},
  {'<leader>qG', ':clast<CR>', desc = 'last'},
  {'<leader>qg', ':cfirst<CR>', desc = 'first'},
  {'<leader>qc', ':cclose<CR>', desc = 'close'},
  {'<leader>qo', ':copen<CR>', desc = 'open'},

  -- make
  {'<leader>m', group = 'make'},
  {'<leader>mm', function() R("funcs.terminal").dedicated("make", {termname = 'make'}) end, desc = 'make'},
  {'<leader>mc', function() R("funcs.terminal").dedicated("make clean", {termname = 'make'}) end, desc = 'clean'},
  {'<leader>ma', function() R("funcs.terminal").dedicated("make all", {termname = 'make'}) end, desc = 'all'},
  {'<leader>mo', function() R("funcs.terminal").dedicated("make open", {termname = 'make'}) end, desc = 'open'},

  -- search
  {'<leader>s', group = 'search'},
  {'<leader>sf', ':lua R("funcs.search").vim("*."..vim.fn.expand("%:e"))<CR>', desc = 'search project'},
  {'<leader>sa', ':lua R("funcs.search").vim("*")<CR>', desc = 'seach project all'},
  {'<leader>ss', ":set operatorfunc=v:lua.require'acksld.funcs.grep'.operator<CR>g@", desc = 'grep operator'},

  -- config
  {'<leader>c', group = 'config'},
  {'<leader>cs', group = 'luasnip'},
  {
    '<leader>csl',
    function()
      package.loaded['acksld.plugins.luasnip'] = nil
      IfHas('acksld.plugins.luasnip', function(m)
        m.config()
      end)
    end,
    desc = 'reload snippets',
  },
  {'<leader>cse', '<Cmd>edit ~/dev/config/nvim/lua/acksld/plugins/luasnip.lua<CR>', desc = 'edit snippets'},

  -- cwd
  {'<leader>nc', group = 'cwd'},
  {'<leader>ncd', ':cd %:p:h<CR>:pwd<CR>', desc = 'cd to current'},
  {'<leader>ncl', ':lcd %:p:h<CR>:pwd<CR>', desc = 'lcd to current'},

  -- unicode
  {'<leader>u', group = 'unicode/color'},
  {'<leader>ut', '<Cmd>lua R("funcs.unicode").replace_hex_to_char()<CR>', desc = 'hx2ch'},
  {'<leader>uf', '<Cmd>lua R("funcs.unicode").replace_char_to_hex()<CR>', desc = 'ch2hx'},
  {'<leader>up', '<Cmd>lua R("colors.colorscheme").pick()<CR>', desc = 'pick colorscheme'},
  {'<leader>ub', '<Cmd>lua R("funcs.highlight").toggle_bright_comments()<CR>', desc = 'toggle bright comments'},

  -- input/language
  {'<leader>i', group = 'input/language'},
  {'<leader>is', '<Cmd>lua R("funcs.language").toggle_swedish()<CR>', desc = 'toggle sv'},
  {'<leader>ic', '<Cmd>lua R("funcs.layout").toggle_colemak()<CR>', desc = 'toggle colemak'},
  {'<leader>in', '<Cmd>set spell!<CR>', desc = 'toggle spell'},

  -- plugin/profile
  {'<leader>p', group = 'plugin'},
  {'<leader>pr', '<Cmd>lua R("plugins")()<CR>', desc = 'load plugins conf'},
  {'<leader>pc', '<Cmd>lua R("funcs.config").compile()<CR>', desc = 'packer compile'},
  {'<leader>pw', function() R('funcs.plugins').workon() end, desc = 'work-on'},
  {'<leader>pf', function() R('funcs.plugins').find_file() end, desc = 'find plugin file'},
  {'<leader>pl', function() R('funcs.plugins').find_line() end, desc = 'find plugin line'},
  {'<leader>pp', group = 'profile'},
  {'<leader>pps', '<Cmd>ProfStart<CR>', desc = 'profile start'},
  {'<leader>ppe', '<Cmd>ProfStop<CR>', desc = 'profile stop'},

  -- navigate
  {'<leader>n', group = 'navigation'},
  {'<leader>nx', '<C-^>', desc = 'alternative file'},

  -- lsp
  {'<leader>l', group = 'lsp'},
  -- show diagnostics
  {'<leader>ld', "<Cmd>lua vim.diagnostic.open_float(0, {scope='line', source=true})<CR>", desc = 'diagnostics'},
  -- toggle diagnostics
  {'<leader>lv', '<Cmd>lua vim.diagnostic.toggle_all()<CR>', desc = 'toggle diagnostics all'},
  -- toggle inlay hints
  {'<leader>lH', '<Cmd>lua vim.lsp.inlay_hint.toggle()<CR>', desc = 'toggle inlay hints'},
  -- toggle bright comments
  {'<leader>lQ', "<Cmd>lua vim.diagnostic.setloclist()<CR>", desc = 'set loclist'},

  -- visual
  {
    mode = 'v',

    {'<leader>K', function()
      local _, start_line, start_col, _ = unpack(vim.fn.getpos('v'))
      local _, end_line, end_col, _ = unpack(vim.fn.getpos('.'))
      local text = vim.api.nvim_buf_get_text(0, start_line - 1, start_col - 1, end_line - 1, end_col, {})[1]
      vim.cmd('help ' .. text)
    end, desc = 'help'},

    -- search
    {'<leader>s', group = 'search'},
    {'<leader>ss', ':call v:lua.grep.operator(visualmode())<CR>', desc = 'grep visual'},
  },
})

-- bracket jump
local brackets = {{'<leader>o', group = 'bracket humps'}}
for _, char in ipairs({'q', 'g', 's', 'n', 'b'}) do
  table.insert(brackets, {
    string.format('<leader>o%s', char),
    function() brj.set(char) end,
    desc = string.format('set bracket jump (%s)', char),
  })
end
wk.add(brackets)
