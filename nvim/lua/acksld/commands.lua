R("funcs.commands").define_commands({
    -- :W sudo saves the file
    -- (useful for handling the permission-denied error)
    W = {command = 'w !sudo tee % > /dev/null'},

    -- profiling
    ProfStart = {command = function() R("funcs.profiling").start("vim-profile.log") end},
    ProfStop = {command = function() R("funcs.profiling").stop() end},

    -- swap
    SwapDiff = {command = function() R("funcs.swap").diff() end},
    SwapClear = {command = function() R("funcs.swap").clear() end},

    -- select file using vifm
    VifmSelectFile = {command = function() R("funcs.vifm").select_to_register() end},

    -- Browse
    Browse = {command = function(opts) R("funcs.browse").browse(opts.args) end, nargs = 1},
})
