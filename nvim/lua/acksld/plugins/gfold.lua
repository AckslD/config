return {
  'AckslD/nvim-gfold.lua',
  lazy = true,
  wkeys = {
    {'<leader>ng', function()
      require('gfold').pick_repo()
    end, desc = 'gfold picker'},
    {'<leader>nG', function()
      require('gfold').pick_repo(function(repo)
        return repo.status ~= 'clean'
      end)
    end, desc = 'gfold picker (non-clean)'},
  },
  config = function()
    if vim.fn.executable('gfold') == 0 then
      return
    end
    require('gfold').setup({
      cwd = vim.fn.expand('~/dev'),
      no_error = true,
    })
  end,
}
