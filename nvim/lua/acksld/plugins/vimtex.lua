return {
  'lervag/vimtex',
  ft = {'tex'},
  init = function()
    vim.g.tex_flavor = 'latex'
    vim.g.vimtex_fold_enabled = 1
  end,
}
