return {
  'jbyuki/venn.nvim',
  cmd = 'VBox',
  setup = 'require("plugin_settings.venn").setup()',
  wkeys = {
    mode = 'v',
    {'<leader>u', group = 'boxes'},
    {'<leader>uv', ':VBox<CR>', desc = 'draw box'},
    {'<leader>ud', ':VBoxD<CR>', desc = 'draw box (double)'},
    {'<leader>uh', ':VBoxH<CR>', desc = 'draw box (thick)'},
    {'<leader>uc', group = 'crossing'},
    {'<leader>ucv', ':VBoxO<CR>', desc = 'draw (crossing) box'},
    {'<leader>ucd', ':VBoxDO<CR>', desc = 'draw (crossing) box (double)'},
    {'<leader>uch', ':VBoxHO<CR>', desc = 'draw (crossing) box (thick)'},
  },
}
