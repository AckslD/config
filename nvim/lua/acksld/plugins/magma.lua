return {
  'dccsillag/magma-nvim',
  -- cmd = 'MagmaInit',
  config = function()
    WK({
      {'<localleader>n', group = 'magma'},
      {'<localleader>nn', '<Cmd>MagmaEvaluateLine<CR>', desc = 'evaluate line'},
      {'<localleader>nc', '<Cmd>MagmaReevaluateCell<CR>', desc = 're-evaluate cell'},
      {'<localleader>nd', '<Cmd>MagmaDelete<CR>', desc = 'delete'},
      {'<localleader>no', '<Cmd>MagmaShowOutput<CR>', desc = 'show output'},
      {'<localleader>ne', '<Cmd>MagmaEvaluateOperator<CR>', desc = 'evaluate operator', expr = true},
      {
        mode = 'x',
        {'<localleader>n', group = 'magma'},
        {'<localleader>ne', ':<C-u>MagmaEvaluateVisual<CR>', desc = 'evaluate visual'},
      },
    })
    vim.g.magma_automatically_open_output = false
    vim.g.magma_image_provider = "kitty"
  end,
}
