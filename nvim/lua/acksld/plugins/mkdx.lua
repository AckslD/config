return {
  'SidOfc/mkdx',
  init = function()
    vim.g["mkdx#settings"] = {
      highlight = {enable = 1},
      enter = {shift = 1},
      links = {
        fragment = {complete = 0},
        external = {enable = 1},
      },
      toc = {text = 'Table of Contents', update_on_write = 1},
      fold = {enable = 0},
    }
  end,
  ft = {'markdown'},
}
