return {
  'machakann/vim-sandwich',
  keys = {'sa', 'sd', 'sr', 'sdb', 'srb'},
  config = function()
    vim.fn['operator#sandwich#set']('replace', 'all', 'cursor', 'keep')
    vim.keymap.set('n', '.', '<Plug>(operator-sandwich-dot)', {desc = 'dot repeat sandwich'})
    vim.g['sandwich#magicchar#f#patterns'] = {
      {
        header = [[\<\%(\h\k*\.\)*\h\k*]],
        bra = '(',
        ket = ')',
        footer = '',
      },
      {
        header = [[\<\%(\h\k*\.\)*\h\k*]],
        bra = '\\[',
        ket = '\\]',
        footer = '',
      },
    }
  end,
}
