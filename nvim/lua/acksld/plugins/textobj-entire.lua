return {
  'kana/vim-textobj-entire',
  keys = {
    {'ae', mode = 'o'},
    {'ie', mode = 'o'},
    {'ae', mode = 'x'},
    {'ie', mode = 'x'},
  },
  dependencies = {'vim-textobj-user'},
}
