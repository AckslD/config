return {
  'karb94/neoscroll.nvim',
  keys = {
    '<C-u>',
    '<C-d>',
    '<C-y>',
    '<C-e>',
    'zt',
    'zz',
    'zb',
  },
  config = function()
    if vim.g.neovide then
      return
    end
    require('neoscroll').setup({
      mappings = {
        '<C-u>',
        '<C-d>',
        '<C-y>',
        '<C-e>',
        'zt',
        'zz',
        'zb',
      },
    })

    local neoscroll = require('neoscroll')
    local keymap = {
      ["<C-u>"] = function() neoscroll.ctrl_u({ duration = 75 }) end,
      ["<C-d>"] = function() neoscroll.ctrl_d({ duration = 75 }) end,
      ["<C-y>"] = function() neoscroll.scroll(-0.1, { move_cursor=false; duration = 30 }) end,
      ["<C-e>"] = function() neoscroll.scroll(0.1, { move_cursor=false; duration = 30 }) end,
      ["zt"]    = function() neoscroll.zt({ half_win_duration = 75 }) end,
      ["zz"]    = function() neoscroll.zz({ half_win_duration = 75 }) end,
      ["zb"]    = function() neoscroll.zb({ half_win_duration = 75 }) end,
    }
    local modes = { 'n', 'v', 'x' }
    for key, func in pairs(keymap) do
      vim.keymap.set(modes, key, func)
    end
  end,
}
