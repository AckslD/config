return {
  'AckslD/swenv.nvim',
  lazy = true,
  wkeys = {
    silent = false,
    -- not really plugin related but hey...
    {'<leader>py', function() require('swenv.api').pick_venv() end, desc = 'pick venv'},
  },
}
