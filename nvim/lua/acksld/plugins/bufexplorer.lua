return {
  'jlanzarotta/bufexplorer',
  cmd = 'BufExplorer',
  init = function()
    vim.g.bufExplorerDisableDefaultKeyMapping = 1
  end,
}
