return {
  'wbthomason/packer.nvim',
  cmd = {
    'PackerLoad',
    'PackerStatus',
    'PackerInstall',
    'PackerSync',
    'PackerUpdate',
    'PackerClean',
    'PackerCompile',
    'PackerProfile',
  },
  wkeys = {
    silent = false,
    {'<leader>p', group = "plugin"},
    {'<leader>pd', "<Cmd>PackerClean<CR>", desc = "packer clean"},
    {'<leader>pi', "<Cmd>PackerInstall<CR>", desc = "packer install"},
    {'<leader>pl', "<Cmd>PackerStatus<CR>", desc = "packer status"},
    {'<leader>ps', "<Cmd>PackerSync --preview<CR>", desc = "packer sync"},
    {'<leader>pS', "<Cmd>PackerSync<CR>", desc = "packer sync (no preview"},
    {'<leader>pu', "<Cmd>PackerUpdate --preview<CR>", desc = "packer update"},
    {'<leader>pU', "<Cmd>PackerUpdate<CR>", desc = "packer update (no preview)"},
  },
}
