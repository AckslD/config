return {
  'mfussenegger/nvim-treehopper',
  keys = {
    {'m', ':<C-U>lua require("tsht").nodes()<CR>', mode = 'o', silent = true},
    {'m', ':lua require("tsht").nodes()<CR>', mode = 'x', silent = true},
  },
}
