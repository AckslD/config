return {
  'nvim-neorg/neorg',
  dependencies = {'nvim-neorg/neorg-telescope'},
  wkeys = {
    {'<leader>a', group = 'neorg'},
    {'<leader>ao', '<Cmd>edit ~/todo/neorg/index.norg<CR>', desc = 'open index'},
    {'<leader>ai', '<Cmd>edit ~/todo/neorg/inbox.norg<CR>', desc = 'open inbox'},
    {'<leader>ac', '<Cmd>Neorg gtd capture<CR>', desc = 'capture'},
    {'<leader>ae', '<Cmd>Neorg gtd edit<CR>', desc = 'edit'},
    {'<leader>av', '<Cmd>Neorg gtd views<CR>', desc = 'views'},
    {'<leader>an', '<Cmd>Neorg news all<CR>', desc = 'news'},
    {'<leader>ap', '<Cmd>Neorg presenter start<CR>', desc = 'start'},
    {'<leader>aP', '<Cmd>Neorg presenter close<CR>', desc = 'close'},
    {'<leader>aw', '<Cmd>Neorg workspaces<CR>', desc = 'workspaces'},
  },
  opts = {
    load = {
      ["core.highlights"] = {
        config = {
          highlights = {
            tags = {
              carryover = {
                begin = "+TSConstant",
                name = {
                  word = "+TSConstant",
                },
              },
            },
          },
        },
      },
      ["core.defaults"] = {},
      ["core.norg.concealer"] = {},
      ["core.export"] = {},
      ["core.gtd.base"] = {
        config = {
          workspace = 'home',
        },
      },
      ["core.gtd.ui"] = {},
      ["core.gtd.helpers"] = {},
      ["core.gtd.queries"] = {},
      ["core.norg.completion"] = {
        config = {
          engine = 'nvim-cmp',
        },
      },
      ["core.integrations.nvim-cmp"] = {},
      ["core.presenter"] = {
        config = {
          zen_mode = "zen-mode",
        },
      },
      ["core.export.markdown"] = {
        config = {
          extensions = "all",
        },
      },
      ["core.norg.dirman"] = {
        config = {
          workspaces = {
            home = "~/todo/neorg",
          },
        },
      },
    },
  },
}
