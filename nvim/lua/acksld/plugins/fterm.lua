return {
  'numtostr/FTerm.nvim',
  lazy = true,
  keys = {
    {'<A-d>', '<Cmd>lua require("FTerm").toggle()<CR>'},
    {'<A-d>', [[<C-\><C-n><Cmd>lua require("FTerm").toggle()<CR>]], mode = 't'},
  },
  wkeys = {
    {'<leader>lg', '<Cmd>lua require("FTerm").lazygit.toggle()<CR>', desc = 'lazygit (float)'},
    {'<leader>bt', '<Cmd>lua require("FTerm").bpytop.toggle()<CR>', desc = 'top (float)'},
    {'<leader>bs', '<Cmd>lua require("FTerm").spt.toggle()<CR>', desc = 'spotify (float)'},
  },
  config = function()
    local fterm = require("FTerm")
    fterm.setup({
      dimensions = {
        height = 0.6,
        width = 0.8,
      },
    })

    local term = require("FTerm.terminal")
    local cmds = {
      lazygit = 'lazygit',
      bpytop = '/usr/bin/python /usr/bin/bpytop',
      spt = 'spt',
    }
    for name, cmd in pairs(cmds) do
      local prog = term:new():setup({
        cmd = cmd,
        dimensions = {
          height = 0.9,
          width = 0.8,
        },
      })
      fterm[name] = {
        toggle = function()
          prog:toggle()
        end,
        open = function()
          prog:open()
        end,
        close = function()
          prog:close()
        end,
      }
    end

    R("funcs.commands").define_commands({
      FTermCloseAll = {command = function()
        fterm.close()
        for name, _ in pairs(cmds) do
          fterm[name].close()
        end
      end}
    })
  end,
}
