return {
  'CKolkey/ts-node-action',
  lazy = true,
  config = function()
    local python_actions = require('ts-node-action.actions.ft.python')
    require("ts-node-action").setup({
      python = {
        list_comprehension = python_actions.expand_list_comprehension,
        set_comprehension = python_actions.expand_set_comprehension,
        dictionary_comprehension = python_actions.expand_dictionary_comprehension,
        for_statement = python_actions.inline_for_statement
      },
    })
  end,
  wkeys = {
    {'<leader>k', function() require('ts-node-action').node_action() end, desc = 'ts node action'},
  },
}
