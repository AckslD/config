return {
  'liuchengxu/vista.vim',
  cmd = 'Vista',
  wkeys = {
    {'<leader>v', '<Cmd>Vista!!<CR>', desc = 'toggle vista'},
  },
}
