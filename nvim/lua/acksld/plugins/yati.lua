return {
  'yioneko/nvim-yati',
  dependencies = {'nvim-treesitter'},
  config = function()
    require("nvim-treesitter.configs").setup({
      yati = {
        enable = true,
        -- disable = {
        --   'lua',
        -- },
      },
      indent = {
        disable = {
          'python',
          'lua',
        },
      },
    })
  end,
}
