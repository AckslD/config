return {
  'aklt/plantuml-syntax',
  ft = {'plantuml'},
  event = {
    'BufReadPre **.plantuml',
    'BufNewFile **.plantuml',
  },
}
