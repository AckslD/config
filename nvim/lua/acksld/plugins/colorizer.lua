return {
  'norcalli/nvim-colorizer.lua',
  cmd = 'ColorizerToggle',
  wkeys = {
    {'<leader>u', group = 'unicode/color'},
    {'<leader>uc', '<Cmd>ColorizerToggle<CR>', desc = 'colorizer'},
  },
  config = true,
}
