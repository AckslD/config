return {
  'beloglazov/vim-textobj-quotes',
  keys = {
    {'aq', mode = 'o'},
    {'iq', mode = 'o'},
    {'aq', mode = 'x'},
    {'iq', mode = 'x'},
  },
  dependencies = {'vim-textobj-user'},
}
