return {
  'AckslD/project.nvim',
  branch = 'on_chdir',
  config = function()
    require('project_nvim').setup({
      silent_chdir = true,
      scope_chdir = 'tab',
      patterns = {".git", "Cargo.toml"},
      -- detection_methods = {'lsp', 'pattern'},
      detection_methods = {'pattern'},
      -- ignore_lsp = {"efm", "null-ls", "jedi_language_server"},
      exclude_dirs = {"/tmp/*"},
      -- ignore = function()
      --   print('filetype', vim.api.nvim_buf_get_option(0, "filetype"))
      --   print('bufname', vim.api.nvim_buf_get_name(0))
      -- end,
      on_chdir = function()
        print('pwd:', vim.fn.system('pwd'))
        vim.fn.system('zoxide add .')
      end,
    })
  end,
}
