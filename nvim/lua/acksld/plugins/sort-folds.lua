return {
  'obreitwi/vim-sort-folds',
  keys = {
    {'<leader>fs', mode = 'x'},
  },
  wkeys = {
    mode='v', noremap=false,
    {'<leader>f', group = 'folds'},
    {'<leader>fs', '<Plug>SortFolds', desc = 'sort'},
  },
  init = function()
    vim.g.sort_folds_ignore_case = true
  end,
}
