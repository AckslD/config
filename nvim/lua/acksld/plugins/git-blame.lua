return {
  'f-person/git-blame.nvim',
  cmd = 'GitBlameToggle',
  wkeys = {
    {'<leader>g', group = 'git'},
    {'<leader>gb', ':GitBlameToggle<CR>', desc = 'blame'},
  },
  config = function()
    vim.g.gitblame_enabled = 0
    vim.g.gitblame_highlight_group = "Comment"
  end
}
