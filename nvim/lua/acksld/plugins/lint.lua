return {
  'mfussenegger/nvim-lint',
  event = 'VeryLazy',
  config = function()
    local lint = require('lint')

    -- update mypy args
    lint.linters.mypy.args = {
      '--show-column-numbers',
      '--show-error-end',
      '--show-error-codes',
      '--no-color-output',
      '--no-error-summary',
      '--no-pretty',
      '--config-file ~/.config/mypy/config_friendly',
    }

    local filter_installed_linters = function(linters)
      local new_linters = {}
      for _, linter in ipairs(linters) do
        if vim.fn.executable(linter) ~= 0 then
          table.insert(new_linters, linter)
        end
      end
      return new_linters
    end

    local filter_installed = function(linters_by_ft)
      for ft, linters in pairs(linters_by_ft) do
        linters_by_ft[ft] = filter_installed_linters(linters)
      end
      return linters_by_ft
    end

    lint.linters_by_ft = filter_installed({
      python = {
        'flake8',
        'mypy',
      },
      lua = {
        'luacheck',
      },
      yaml = {
        'yamllint',
      },
      -- json = {
      --     'jsonlint',
      -- },
      markdown = {
        -- 'markdownlint',
        'proselint',
      },
      tex = {
        'proselint',
      },
      vim = {
        'vint',
      },
      -- make = {
      --     'checkmake',
      -- },
      rst = {
        'rstlint',
      },
      dockerfile = {
        'hadolint',
      },
      sh = {
        'shellcheck',
      },
      nix = {
        'nix',
      },
    })

    local event = {'BufEnter', 'BufWritePost'}
    local all_ft_linters = filter_installed_linters({"codespell"})

    local linters_cache = {}
    local get_linters = function(filetype)
      if linters_cache[filetype] == nil then
        local linters = lint._resolve_linter_by_ft(filetype)
        vim.list_extend(linters, all_ft_linters)
        linters_cache[filetype] = linters
      end
      return linters_cache[filetype]
    end
    R('funcs.autocmds').create_grouped_autocmds({
      MyLint = {
        [event] = {
          callback = function()
            if vim.bo.modifiable and vim.bo.filetype ~= '' then
              lint.try_lint(get_linters(vim.bo.filetype))
            end
          end
        },
      },
    })
  end,
}
