return {
  'bfredl/nvim-ipy',
  cmd = {'IPython'},
  init = function()
    vim.g.nvim_ipy_perform_mappings = 0
    vim.g.ipy_celldef = '^##'

    R("funcs.commands").define_commands({
      RunQTConsole = {command = function()
        vim.fn.jobstart('jupyter qtconsole --JupyterWidget.include_other_output=True')
        vim.defer_fn(function()
          vim.cmd.IPython('--existing', '--no-window')
        end, 1000)
      end, buffer = 0},
    })

    R('funcs.autocmds').create_grouped_autocmds({
      MyIPy = {
        FileType = {
          pattern = 'python',
          callback = function()
            WK({
              {
                silent = true, buffer = 0,
                {'<localleader>n', group = 'ipython'},
                {'<localleader>no', function() vim.cmd.RunQTConsole() end, desc = 'start qt and connect'},
                {'<localleader>nc', function() vim.cmd.IPython('--existing', '--no-window') end, desc = 'connect to existing'},
                {'<localleader>nn', '<Plug>(IPy-Run)', desc = 'run line'},
                {'<localleader>nr', '<Plug>(IPy-RunCell)', desc = 'run cell'},
                {'<localleader>na', '<Plug>(IPy-RunAll)', desc = 'run all'},
                {'<localleader>nt', '<Plug>(IPy-RunOp)', desc = 'run op'},
                {'<localleader>nk', '<Plug>(IPy-WordObjInfo)', desc = 'info cursor'},
                {'<localleader>nx', '<Plug>(IPy-Interrupt)', desc = 'interrupt'},
                {'<localleader>nq', '<Plug>(IPy-Terminate)', desc = 'terminate'},
              },
              {
                mode = 'x', silent = true, buffer = 0,
                {'<localleader>n', group = 'ipython'},
                {'<localleader>nn', '<Plug>(IPy-Run)', desc = 'run visual'},
              },
            })
          end,
        },
      },
    })
  end,
}
