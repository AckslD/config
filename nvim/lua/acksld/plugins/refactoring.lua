return {
  "ThePrimeagen/refactoring.nvim",
  lazy = true,
  init = function()
    WK({
      {
        mode = 'v',
        {'<leader>r', group = 'refactor'},
        {'<leader>re', [[ <Esc><Cmd>lua require('refactoring').refactor('Extract Function')<CR>]], desc = 'extract function'},
        {'<leader>rf', [[ <Esc><Cmd>lua require('refactoring').refactor('Extract Function To File')<CR>]], desc = 'extract to file'},
        {'<leader>rv', [[ <Esc><Cmd>lua require('refactoring').refactor('Extract Variable')<CR>]], desc = 'extract variable'},
        {'<leader>ri', [[ <Esc><Cmd>lua require('refactoring').refactor('Inline Variable')<CR>]], desc = 'inline variable'},
        {'<leader>rt', [[ <Esc><Cmd>lua require('refactoring').select_refactor()<CR>]], desc = 'select refactors'},
      },
      {'<leader>r', group = 'refactor'},
      {'<leader>rb', [[ <Esc><Cmd>lua require('refactoring').refactor('Extract Block')<CR>]], desc = 'extract block'},
      {'<leader>rB', [[ <Esc><Cmd>lua require('refactoring').refactor('Extract Block To File')<CR>]], desc = 'extract block to file'},
      {'<leader>ri', [[ <Esc><Cmd>lua require('refactoring').refactor('Inline Variable')<CR>]], desc = 'inline variable'},
    })
  end,
  config = true,
}
