return {
  'petertriho/cmp-git',
  lazy = true,
  opts = {
    filetypes = { "gitcommit", "markdown" },
  },
}
