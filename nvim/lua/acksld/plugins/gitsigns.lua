return {
  'lewis6991/gitsigns.nvim',
  event = 'VeryLazy',
  dependencies = {'plenary.nvim'},
  wkeys = {
    {'<leader>h', group = 'hunk'},
    {'<leader>hs', '<Cmd>lua require("gitsigns").stage_hunk()<CR>', desc = 'stage'},
    {'<leader>hu', '<Cmd>lua require("gitsigns").undo_stage_hunk()<CR>', desc = 'unstage'},
    {'<leader>hr', '<Cmd>lua require("gitsigns").reset_hunk()<CR>', desc = 'reset'},
    {'<leader>hR', '<Cmd>lua require("gitsigns").reset_buffer()<CR>', desc = 'reset buffer'},
    {'<leader>hp', '<Cmd>lua require("gitsigns").preview_hunk()<CR>', desc = 'preview'},
    {'<leader>hb', '<Cmd>lua require("gitsigns").blame_line()<CR>', desc = 'blame'},
    {'<leader>ht', '<Cmd>lua require("gitsigns").toggle_signs()<CR>', desc = 'toggle signs'},
  },
  config = function()
    vim.cmd('highlight link GitSignsAdd Title')
    vim.cmd('highlight link GitSignsDelete WarningMsg')
    vim.cmd('highlight link GitSignsChange ModeMsg')

    require('gitsigns').setup {
      signs = {
        add          = {text = '│'},
        change       = {text = '│'},
        delete       = {text = '_'},
        topdelete    = {text = '‾'},
        changedelete = {text = '~'},
      },
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns
        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        -- Navigation
        map('n', ']c', function()
          if vim.wo.diff then return ']c' end
          vim.schedule(function() gs.next_hunk() end)
          return '<Ignore>'
        end, {expr=true})

        map('n', '[c', function()
          if vim.wo.diff then return '[c' end
          vim.schedule(function() gs.prev_hunk() end)
          return '<Ignore>'
        end, {expr=true})

        -- Text object
        map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
      end,
    }
  end,
}
