return {
  'AckslD/todo.txt-vim',
  branch = 'develop',
  dependencies = {
    'junegunn/fzf',
    -- 'junegunn/fzf.vim',
  },
  -- ft = 'todo',
  config = function()
    vim.g.todo_load_python = true
    vim.g.todo_focus_initial_projects = {'work'}
    R('funcs.autocmds').create_grouped_autocmds({
      MyIPy = {
        FileType = {
          pattern = 'todo',
          callback = function()
            WK({  -- TODO how to handle?
            {
              buffer = 0,
              {'<localleader>s', group = 'sort'},
              {'<localleader>ss', desc = 'sort'},
              {'<localleader>s@', desc = 'sort_by_context'},
              {'<localleader>s+', desc = 'sort_by_project'},

              {'<localleader>sd', group = 'date'},
              {'<localleader>sds', desc = 'by_date'},
              {'<localleader>sdd', desc = 'by_due_date'},

              {'<localleader>j', desc = 'prioritize_increase'},
              {'<localleader>k', desc = 'prioritize_decrease'},
              {'<localleader>a', desc = 'prioritize_add A'},
              {'<localleader>b', desc = 'prioritize_add B'},
              {'<localleader>c', desc = 'prioritize_add C'},
              {'<localleader>d', desc = 'date pick_date'},
              {'<localleader>x', desc = 'mark_as_done'},
              {'<localleader>X', desc = 'mark_all_as_done'},
              {'<localleader>D', desc = 'remove_completed'},

              {'<localleader>n', group = 'notes'},
              {'<localleader>no', desc = 'open'},
              {'<localleader>nd', desc = 'pop'},

              {'<localleader>t', group = 'subtasks'},
              {'<localleader>to', desc = 'open'},
              {'<localleader>td', desc = 'pop'},

              {'<localleader>l', group = 'links'},
              {'<localleader>lo', desc = 'open'},
              {'<localleader>ld', desc = 'pop'},

              {'<localleader>v', group = 'todokeys'},
              {'<localleader>vd', desc = 'todokeys pop'},

              {'<localleader>o', desc = 'tasks insert_new("n", 0, 0)'},
              {'<localleader>O', desc = 'tasks insert_new("n", 1, 0)'},
              {'<localleader>p', desc = 'tasks insert_new("n", 0, 1)'},
              {'<localleader>P', desc = 'tasks insert_new("n", 1, 1)'},
              {'<localleader>m', desc = 'toggle_backlog'},

              {'<localleader>z', group = 'folding'},
              {'<localleader>zp', desc = 'toggle_focus_project'},
              {'<localleader>zc', desc = 'toggle_focus_context'},
              {'<localleader>zd', desc = 'toggle_focus_due_date'},
              {'<localleader>zt', desc = 'focus_query_tag'},
            },

            {
              buffer = 0, mode = 'v',
              {'<localleader>s', group = 'sort'},
              {'<localleader>ss', desc = 'sort'},
              {'<localleader>s@', desc = 'sort_by_context'},
              {'<localleader>s+', desc = 'sort_by_project'},

              {'<localleader>sd', group = 'date'},
              {'<localleader>sds', desc = 'by_date'},
              {'<localleader>sdd', desc = 'by_due_date'},

              {'<localleader>c', desc = 'prioritize_add C'},
              {'<localleader>b', desc = 'prioritize_add B'},
              {'<localleader>a', desc = 'prioritize_add A'},
              {'<localleader>k', desc = 'prioritize_decrease'},
              {'<localleader>j', desc = 'prioritize_increase'},
              {'<localleader>x', desc = 'mark_as_done'},
              {'<localleader>m', desc = 'toggle_backlog(visualmode())'},
            },
          })
        end,
      },
    },
  })
end,
}
