return {
  'nvim-lua/plenary.nvim',
  cmd = 'PlenaryBustedDirectory',
  wkeys = {
    {'<leader>p', group = "plugin"},
    {'<leader>pt', "<Plug>PlenaryTestFile", desc = "test file"},
  },
}
