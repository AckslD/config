return {
  "danymat/neogen",
  lazy = true,
  module = 'neogen',
  wkeys = {
    -- NOTE jump commands defined in luasnip config
    {'<leader>lD', '<Cmd>lua require("neogen").generate()<CR>', desc = 'docs'},
  },
  opts = {
    enabled = true,
  },
  config = 'require("plugin_settings.neogen").config()',
}
