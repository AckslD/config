return {
  'gabrielpoca/replacer.nvim',
  lazy = true,
  wkeys = {
    {'<leader>q', group = 'quickfix'},
    {'<leader>qr', '<Cmd>lua require("replacer").run()<CR>', desc = 'replacer'},
  },
}
