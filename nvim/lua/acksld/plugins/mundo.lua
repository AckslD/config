return {
  'simnalamburt/vim-mundo',
  cmd = 'MundoToggle',
  wkeys = {
    {'<leader>U', ':MundoToggle<CR>', desc = 'mundo toggle'},
  },
}
