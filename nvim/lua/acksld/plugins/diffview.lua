return {
  'sindrets/diffview.nvim',
  cmd = {
    'DiffviewOpen',
    'DiffviewClose',
    'DiffviewFileHistory',
    'DiffviewToggleFiles',
    'DiffviewFocusFiles',
    'DiffviewLog',
  },
  init = function()
    WK({
      {'<leader>g', group = 'git'},
      {'<leader>gd', '<Cmd>DiffViewOpen<CR>', desc = 'diff view open'},
      {'<leader>gq', '<Cmd>DiffViewClose<CR>', desc = 'diff view close'},
      {'<leader>gh', '<Cmd>DiffViewFileHistory<CR>', desc = 'diff view file history'},
      {'<leader>gf', '<Cmd>DiffViewToggleFiles<CR>', desc = 'diff view toggle panel'},
      {'<leader>gp', '<Cmd>DiffViewFocusFiles<CR>', desc = 'diff view focus panel'},
      {'<leader>gl', '<Cmd>DiffViewLog<CR>', desc = 'diff view log'},
      {
        mode = 'v',
        {'<leader>g', group = 'git'},
        {'<leader>gh', '<Cmd>DiffViewFileHistory<CR>', desc = 'diff view file history'},
      },
    })
  end,
}
