return {
  'simrat39/rust-tools.nvim',
  ft = 'rust',
  opts = {
    server = {
      on_attach = R('funcs.lsp').get_on_attach('rust_analyzer'),
      ["rust-analyzer"] = {
        procMacros = {
          enabled = true,
        },
      },
    },
  },
}
