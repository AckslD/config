return {
  'code-biscuits/nvim-biscuits',
  lazy = true,
  wkeys = {
    {'<leader>lb', '<Cmd>lua require("nvim-biscuits").toggle_biscuits()<CR>', desc = 'toggle biscuits'},
  },
  opts = {
    default_config = {
      max_length = 12,
      min_distance = 5,
      -- prefix_string = " 📎 ",
    },
  },
}
