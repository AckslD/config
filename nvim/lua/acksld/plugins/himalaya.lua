return {
  'soywod/himalaya',
  cmd = 'Himalaya',
  wkeys = {
    {'<leader>bm', '<Cmd>Himalaya<CR>', desc = 'open'},
  },
  init = function()
    vim.g.himalaya_complete_contact_cmd = "khard email --remove-first-line --parsable '%s'"
  end,
  config = function(plugin)
    vim.opt.rtp:append(plugin.dir .. "/vim")
    R('funcs.autocmds').create_grouped_autocmds({
      MyHimalaya = {
        FileType = {
          callback = function()
            WK({
              {'<localleader>m', '<Plug>(himalaya-mbox-input)', desc = 'change mailbox'},
              {'<localleader>p', '<Plug>(himalaya-mbox-prev-page)', desc = 'prev page'},
              {'<localleader>n', '<Plug>(himalaya-mbox-next-page)', desc = 'next page'},
              {'<localleader>w', '<Plug>(himalaya-msg-write)', desc = 'write'},
              {'<localleader>R', '<Plug>(himalaya-msg-reply)', desc = 'reply'},
              {'<localleader>r', '<Plug>(himalaya-msg-reply-all)', desc = 'reply all'},
              {'<localleader>f', '<Plug>(himalaya-msg-forward)', desc = 'forward'},
              {'<localleader>a', '<Plug>(himalaya-msg-attachments)', desc = 'attachments'},
            })
          end,
          pattern = 'himalaya' -- TODO is this the filetype?
        },
      },
    })
  end,
}
