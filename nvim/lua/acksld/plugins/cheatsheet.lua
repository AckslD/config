return {
  'sudormrfbin/cheatsheet.nvim',
  cmd = 'Cheatsheet',
  wkeys = {
    {'<leader>K', ':Cheatsheet<CR>', desc = 'cheatsheet'},
  },
}
