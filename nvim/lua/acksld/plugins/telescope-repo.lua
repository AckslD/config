local fd_opts = {
  '--exclude', '.cache',
  '--exclude', 'tmp',
  '--exclude', '.config',
}

return {
  'cljoly/telescope-repo.nvim',
  lazy = true,
  wkeys = {
    {'<leader>fp', string.format('<Cmd>lua require("telescope").extensions.repo.list({fd_opts=%s})<CR>', vim.inspect(fd_opts)), desc = 'projects'},
    {'<leader>fP', '<Cmd>lua require("telescope").extensions.repo.cached_list()<CR>', desc = 'projects (cached)'},
  },
}
