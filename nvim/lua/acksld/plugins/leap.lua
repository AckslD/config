return {
  'ggandor/leap.nvim',
  config = function()
    -- vim.keymap.set('n', 'f', '<Plug>(leap-forward-to)', {desc = 'leap forward to'})
    -- vim.keymap.set('n', 'F', '<Plug>(leap-backward-to)', {desc = 'leap backward to'})
    vim.keymap.set('n', 'g<CR>', '<Plug>(leap-from-window)', {desc = 'leap from window'})
    -- vim.keymap.set({'n', 'x', 'o'}, 'f', '<Plug>(leap-forward-to)', {desc = 'leap forward to'})
    -- vim.keymap.set({'n', 'x', 'o'}, 'F', '<Plug>(leap-backward-to)', {desc = 'leap backward to'})
    -- vim.keymap.set({'n', 'x', 'o'}, 'g<CR>', '<Plug>(leap-from-window)', {desc = 'leap from window'})
    -- vim.keymap.set({'x', 'o'}, 'f', '<Plug>(leap-forward-till)', {desc = 'leap forward till'})
    -- vim.keymap.set({'x', 'o'}, 'F', '<Plug>(leap-backward-till)', {desc = 'leap backward till'})
  end,
}
