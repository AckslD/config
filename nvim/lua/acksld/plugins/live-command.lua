return {
  'smjonas/live-command.nvim',
  event = 'CmdlineEnter',
  config = function()
    if vim.fn.has("nvim-0.8.0") ~= 1 then
      return
    end
    require("live-command").setup {
      commands = {
        Norm = {cmd = "norm"},
        -- TODO should be used differently now?
        -- Q = {cmd = "norm", args = '@q'},
        -- G = {cmd = 'g' , hl_range = {kind = "visible"}}
      },
      enable_highlighting = true,
      hl_groups = {
        insertion = "DiffAdd",
        replacement = "DiffChange",
        deletion = "DiffDelete",
      },
      hl_range = { 0, 0, kind = "relative" },
    }
  end,
}
