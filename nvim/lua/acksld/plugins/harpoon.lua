return {
  'ThePrimeagen/harpoon',
  lazy = true,
  dependencies = {
    'popup.nvim',
    'plenary.nvim',
  },
  keys = {
    {'<A-j>', '<Cmd>lua require("harpoon.ui").nav_next()<CR>', {desc = 'harpoon next'}},
    {'<A-k>', '<Cmd>lua require("harpoon.ui").nav_prev()<CR>', {desc = 'harpoon prev'}},
  },
  wkeys = {
    {'<leader>e', group = 'harpoon'},
    {'<leader>en', '<Cmd>lua require("harpoon.mark").add_file()<CR>', desc = 'add file'},
    {'<leader>em', '<Cmd>lua require("harpoon.ui").toggle_quick_menu()<CR>', desc = 'toggle menu'},
    {'<leader>eq', '<Cmd>lua require("harpoon.ui").nav_file(1)<CR>', desc = 'nav file 1'},
    {'<leader>ew', '<Cmd>lua require("harpoon.ui").nav_file(2)<CR>', desc = 'nav file 2'},
    {'<leader>ef', '<Cmd>lua require("harpoon.ui").nav_file(3)<CR>', desc = 'nav file 3'},
    {'<leader>ep', '<Cmd>lua require("harpoon.ui").nav_file(4)<CR>', desc = 'nav file 4'},
    {'<leader>ea', '<Cmd>lua require("harpoon.term").gotoTerminal(1)<CR>', desc = 'term 1'},
    {'<leader>er', '<Cmd>lua require("harpoon.term").gotoTerminal(2)<CR>', desc = 'term 2'},
    {'<leader>es', '<Cmd>lua require("harpoon.term").gotoTerminal(3)<CR>', desc = 'term 3'},
    {'<leader>et', '<Cmd>lua require("harpoon.term").gotoTerminal(4)<CR>', desc = 'term 4'},
    {'<leader>ez', '<Cmd>lua require("harpoon.term").sendCommand(1)<CR>', desc = 'send 1'},
    {'<leader>ex', '<Cmd>lua require("harpoon.term").sendCommand(2)<CR>', desc = 'send 2'},
    {'<leader>ec', '<Cmd>lua require("harpoon.term").sendCommand(3)<CR>', desc = 'send 3'},
    {'<leader>ev', '<Cmd>lua require("harpoon.term").sendCommand(4)<CR>', desc = 'send 4'},
  },
}
