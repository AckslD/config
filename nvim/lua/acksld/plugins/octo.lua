return {
  'pwntester/octo.nvim',
  cmd = 'Octo',
  wkeys = {
    {'<leader>go', ':Octo<CR>', desc = 'octo'},
  },
  config = true,
}
