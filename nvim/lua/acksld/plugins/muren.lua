return {
  'AckslD/muren.nvim',
  config = true,
  wkeys = {
    {'<leader>sm', '<Cmd>MurenToggle<CR>', desc = 'muren toggle'},
  },
}
