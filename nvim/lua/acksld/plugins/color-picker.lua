return {
  'ziontee113/color-picker.nvim',
  cmd = 'PickColor',
  opts = {
    border = 'rounded'
  },
}
