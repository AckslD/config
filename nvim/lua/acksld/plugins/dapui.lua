return {
  'rcarriga/nvim-dap-ui',
  dependencies = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"},
  lazy = true,
  dependencies = {
    'nvim-neotest/nvim-nio',
  },
  init = function()
    WK({
      {'<leader>d', group = 'debug'},
      {'<leader>du', '<Cmd>lua require"dapui".toggle()<CR>', desc = 'ui toggle'},
      {'<leader>de', '<Cmd>lua require"dapui".eval()<CR>', desc = 'eval'},
      {'<leader>dE', '<Cmd>lua require"dapui".toggle()<CR>', desc = 'float element'},
      {
        mode = 'v',
        {'<leader>d', group = 'debug'},
        {'<leader>de', '<Cmd>lua require"dapui".eval()<CR>', desc = 'eval'},
      },
    })
  end,
  config = true,
}
