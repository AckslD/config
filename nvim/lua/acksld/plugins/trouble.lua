return {
  'folke/trouble.nvim',
  dependencies = {'nvim-web-devicons'},
  cmd = {'Trouble', 'TroubleClose', 'TroubleToggle', 'TroubleRefresh'},
  wkeys = {
    {'<leader>z', group = 'trouble'},
    {'<leader>zz', '<Cmd>TroubleToggle<CR>', desc = 'toggle'},
    {'<leader>zw', '<Cmd>TroubleToggle workspace_diagnostics<CR>', desc = 'workspace'},
    {'<leader>zd', '<Cmd>TroubleToggle document_diagnostics<CR>', desc = 'diagnostics'},
    {'<leader>zq', '<Cmd>TroubleToggle quickfix<CR>', desc = 'quickfix'},
    {'<leader>zl', '<Cmd>TroubleToggle loclist<CR>', desc = 'loclist'},
    {'<leader>zr', '<Cmd>TroubleToggle lsp_references<CR>', desc = 'references'},
  },
  config = true,
}
