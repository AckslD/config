return {
  "andrewferrier/debugprint.nvim",
  lazy = true,
  wkeys = {
    {'g/', group = 'debugprint'},
    {
      expr = true,
      {'g/p', function() return require('debugprint').debugprint() end, desc = 'plain'},
      {'g/P', function() return require('debugprint').debugprint({ above = true }) end, desc = 'plain above'},
      {'g/v', function() return require('debugprint').debugprint({ variable = true }) end, desc = 'variable'},
      {'g/V', function() return require('debugprint').debugprint({ above = true, variable = true }) end, desc = 'variable above'},
      {'g/o', function() return require('debugprint').debugprint({ motion = true }) end, desc = 'operator'},
      {'g/O', function() return require('debugprint').debugprint({ above = true, motion = true }) end, desc = 'operator above'},
    },
    {'g/d', function() require('debugprint').deleteprints() end, desc = 'delete'},
  },
  opts = {
    create_keymaps = false,
  },
}
