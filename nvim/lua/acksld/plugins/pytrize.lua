return {
  'AckslD/nvim-pytrize.lua',
  cmd = {'Pytrize', 'PytrizeClear', 'PytrizeJump', 'PytrizeJumpFixture'},
  init = function()
    R('funcs.autocmds').create_grouped_autocmds({
      MyPytrize = {
        [{'BufEnter', 'BufWritePost'}] = {
          pattern = 'test_*.py',
          callback = function()
            require('pytrize.api').set()
          end
        },
      },
    })
  end,
  opts = {
    no_commands = false,
    highlight = 'LineNrAbove',
  }
}
