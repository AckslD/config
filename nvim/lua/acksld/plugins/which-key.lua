return {
  'folke/which-key.nvim',
  lazy = true,
  config = function()
    local wk = require('which-key')
    wk.setup({
      plugins = {
        spelling = {enabled = true},
        presets = {
          operators = false,
          motions = false,
          windows = false,
        },
      },
      filter = function(mapping)
        return mapping.desc and mapping.desc ~= ''
      end,
    })
  end,
}
