local leader = vim.g.mapleader or [[\]]
return {
  'phaazon/hop.nvim',
  cmd = {
    'HopChar2',
    'HopChar1',
    'HopChar2',
    'HopWord',
    'HopLine',
    'HopPattern',
    'HopWord',
  },
  keys = {
    {'<CR>', '<Cmd>HopWord<CR>'},
  },
  wkeys = {
    silent = false,
    {'<leader>C', "<Cmd>HopChar2<CR>", desc = "hop char2"},
    {string.format('<leader>%s', leader), group = "hop"},
    {string.format('<leader>%sf', leader), "<Cmd>HopChar1<CR>", desc = "hop char1"},
    {string.format('<leader>%sc', leader), "<Cmd>HopChar2<CR>", desc = "hop char2"},
    {string.format('<leader>%sw', leader), "<Cmd>HopWord<CR>", desc = "hop word"},
    {string.format('<leader>%sl', leader), "<Cmd>HopLine<CR>", desc = "hop line"},
    {string.format('<leader>%sp', leader), "<Cmd>HopPattern<CR>", desc = "hop pattern"},
  },
  init = function()
    local unmap_cr = function()
      vim.api.nvim_buf_set_keymap(0, "n", "<CR>", "<CR>", {noremap = true})
    end
    R('funcs.autocmds').create_grouped_autocmds({
      MyHop = {
        CmdwinEnter = {
          callback = unmap_cr,
        },
        FileType = {
          callback = unmap_cr,
          pattern = 'qf',
        },
      },
    })
  end,
  config = true,
}
