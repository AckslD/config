return {
  'windwp/nvim-spectre',
  dependencies = {'popup.nvim', 'plenary.nvim'},
  lazy = true,
  wkeys = {
    {'<leader>s', group = 'search'},
    {'<leader>sp', ':lua require("spectre").open()<CR>', desc = 'spectre'},
  },
  config = true,
}
