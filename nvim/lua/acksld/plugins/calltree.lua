return {
  'ldelossa/litee-calltree.nvim',
  dependencies = {
    'ldelossa/litee.nvim',
  },
  lazy = true,
  config = function()
    require('litee.calltree').setup()
  end
}
