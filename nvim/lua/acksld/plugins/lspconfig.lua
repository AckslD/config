return {
  'neovim/nvim-lspconfig',
  -- event = 'VeryLazy',
  -- commit = '212b99bc12a5416df8b2a610711ba399e2fc388a',
  -- commit = '3e2cc7061957292850cc386d9146f55458ae9fe3',
  dependencies = {
    'ray-x/lsp_signature.nvim',
    -- 'nvim-lsp-notify',
    -- 'renamer.nvim',
  },
  config = function()
    IfHas('neodev', function(neodev)
      neodev.setup()
    end)

    local nvim_lsp = require('lspconfig')

    local capabilities = require('cmp_nvim_lsp').default_capabilities()
    -- -- client capabilities
    -- local capabilities = vim.lsp.protocol.make_client_capabilities()
    --
    -- -- for cmp lsp
    -- local completionItem = capabilities.textDocument.completion.completionItem
    -- completionItem.snippetSupport = true
    -- completionItem.preselectSupport = true
    -- completionItem.insertReplaceSupport = true
    -- completionItem.labelDetailsSupport = true
    -- completionItem.deprecatedSupport = true
    -- completionItem.commitCharactersSupport = true
    -- completionItem.tagSupport = { valueSet = { 1 } }
    -- completionItem.resolveSupport = {
    --   properties = {
    --     'documentation',
    --     'detail',
    --     'additionalTextEdits',
    --   }
    -- }

    local border = 'rounded'
    local handlers =  {
      ["textDocument/hover"] =  vim.lsp.with(vim.lsp.handlers.hover, {border = border}),
      ["textDocument/signatureHelp"] =  vim.lsp.with(vim.lsp.handlers.signature_help, {border = border }),
    }

    local current_rename_handler = vim.lsp.handlers['textDocument/rename']
    vim.lsp.handlers['textDocument/rename'] = function(err, result, ctx, config)
      current_rename_handler(err, result, ctx, config)
      pcall(
        vim.fn["repeat#set"],
        vim.api.nvim_replace_termcodes(
          string.format([[<Cmd>lua vim.lsp.buf.rename('%s')<CR>]], ctx.params.newName),
          true,
          false,
          true
        )
      )
    end
    local current_definition_handler = vim.lsp.handlers['textDocument/definition']
    vim.lsp.handlers['textDocument/definition'] = function(err, result, ctx, config)
      if not result then
        vim.notify("couldn't find definition")
      end
      current_definition_handler(err, result, ctx, config)
    end

    -- Use a loop to conveniently both setup defined servers
    -- and map buffer local keybindings when the language server attaches
    local servers = { -- {{{
      -- pylyzer = {},
      basedpyright = {
        single_file_support = true,
        flags = {
          debounce_text_changes = 150,
        },
        settings = {
          basedpyright = {
            analysis = {
              -- reportGeneralTypeIssues = 'information',
              typeCheckingMode = 'off',
            },
          },
        },
      },
      jedi_language_server = {
        single_file_support = true,
      },
      -- ruff_lsp = {},
      -- rust_analyzer = {
      --   settings = {
      --     ["rust-analyzer"] = {
      --       procMacro = {
      --         enable = true,
      --       },
      --     }
      --   }
      -- },
      vimls = {},
      bashls = {},
      dockerls = {},
      jsonls = {},
      kotlin_language_server = {},
      ts_ls = {},
      texlab = {},
      yamlls = {},
      html = {},
      ccls = {
        init_options = {compilationDatabaseDirectory = "build"},
      },
      lemminx = {
        cmd = {'lemminx'},
      },
      lua_ls = { -- {{{
        cmd = {'lua-language-server'},
        settings = {
          Lua = {
            runtime = {
              version = 'LuaJIT',
              path = vim.split(package.path, ';'),
            },
            diagnostics = {globals = {'vim', 'P', 'R'}},
            workspace = {
              library = {
                [vim.fn.expand('$VIMRUNTIME/lua')] = true,
                [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true,
              },
            },
          },
        },
      }, -- }}}
      gdscript = {
        cmd = vim.lsp.rpc.connect('127.0.0.1', 6005),
      },
      -- efm = { -- linters + formatters {{{
      --     cmd = {'efm-langserver'},
      --     init_options = {documentFormatting = false},
      --     filetypes = {
      --         "lua",
      --         "python",
      --         "vim",
      --         "make",
      --         "markdown",
      --         "rst",
      --         "yaml",
      --         "dockerfile",
      --         "sh",
      --         "json",
      --     },
      --     root_dir = nvim_lsp.util.root_pattern{".git/", "."},
      -- }, -- }}}
    } -- }}}
    for lsp, setup in pairs(servers) do
      setup.on_attach = R('funcs.lsp').get_on_attach(lsp)
      setup.capabilities = capabilities
      setup.handlers = handlers
      nvim_lsp[lsp].setup(setup)
    end

    -- colors and icons {{{
    -- TODO move elsewhere?
    vim.cmd('highlight! link LspDiagnosticsDefaultError WarningMsg')
    vim.fn.sign_define("LspDiagnosticsSignError", {
      texthl = "LspDiagnosticsSignError",
      text = "",
      numhl = "LspDiagnosticsSignError",
    })
    vim.fn.sign_define("LspDiagnosticsSignWarning", {
      texthl = "LspDiagnosticsSignWarning",
      text = "",
      numhl = "LspDiagnosticsSignWarning"
    })
    vim.fn.sign_define("LspDiagnosticsSignInformation", {
      texthl = "LspDiagnosticsSignInformation",
      text = "",
      numhl = "LspDiagnosticsSignInformation"
    })
    vim.fn.sign_define("LspDiagnosticsSignHint", {
      texthl = "LspDiagnosticsSignHint",
      text = "",
      numhl = "LspDiagnosticsSignHint"
    })
    -- }}}
  end
}
