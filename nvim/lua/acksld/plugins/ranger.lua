return {
  'francoiscabrol/ranger.vim',
  cmd = {'Ranger'},
  keys = {
    {'<C-n>', ':Ranger<CR>', silent = true},
  },
  wkeys = {
    {'<leader>n', group = 'navigation'},
    {'<leader>nd', ':DiffVifm<CR>', desc = 'diff files'},
    {'<leader>ns', ':SplitVifm<CR>', desc = 'new file split'},
    {'<leader>nv', ':VsplitVifm<CR>', desc = 'new file v split'},
    {'<leader>nt', ':TabVifm<CR>', desc = 'new file tab'},
    {'<leader>nf', '<Cmd>VifmSelectFile<CR>', desc = 'vifm select'},
  },
}
