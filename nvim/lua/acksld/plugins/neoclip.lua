local function is_whitespace(line)
    return vim.fn.match(line, [[^\s*$]]) ~= -1
end

local function all(tbl, check)
    for _, entry in ipairs(tbl) do
        if not check(entry) then
            return false
        end
    end
    return true
end

return {
  'AckslD/nvim-neoclip.lua',
  event = 'VeryLazy',
  dependencies = {'tami5/sqlite.lua'},
  wkeys = {
    {'<leader>fx', '<Cmd>lua require("telescope").extensions.neoclip.default()<CR>', desc = 'neoclip'},
    {'<leader>fm', '<Cmd>lua require("telescope").extensions.macroscope.default()<CR>', desc = 'macroscope'},
    {'<leader>fzx', '<Cmd>lua require("neoclip.fzf")()<CR>', desc = 'neoclip'},
  },
  opts = {
    history = 50,
    enable_persistent_history = true,
    continuous_sync = true,
    filter = function(data)
      return not all(data.event.regcontents, is_whitespace)
    end,
  },
}
