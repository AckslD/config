return {
  "SmiteshP/nvim-navbuddy",
  dependencies = {
    'MunifTanjim/nui.nvim',
    'SmiteshP/nvim-navic',
  },
  lazy = true,
  config = {
    lsp = {
      preference = {
        'pyright',
      },
    },
  },
  wkeys = {
    {'<leader>ln', '<Cmd>lua require("nvim-navbuddy").open()<CR>', desc = 'navbuddy'},
  },
}
