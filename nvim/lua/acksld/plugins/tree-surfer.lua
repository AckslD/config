return {
  'ziontee113/syntax-tree-surfer',
  keys = {
  -- NAVIGATION: Only change the keymap to your liking. I would not recommend changing anything about the .surf() parameters!
    {"<A-l>", '<Cmd>lua require("syntax-tree-surfer").surf("next", "visual")<CR>', mode = 'x', noremap = true, silent = true},
    {"<A-h>", '<Cmd>lua require("syntax-tree-surfer").surf("prev", "visual")<CR>', mode = 'x', noremap = true, silent = true},
    {"<A-k>", '<Cmd>lua require("syntax-tree-surfer").surf("parent", "visual")<CR>', mode = 'x', noremap = true, silent = true},
    {"<A-j>", '<Cmd>lua require("syntax-tree-surfer").surf("child", "visual")<CR>', mode = 'x', noremap = true, silent = true},

  -- SWAPPING WITH VISUAL SELECTION: Only change the keymap to your liking. Don't change the .surf() parameters!
  {"L", '<Cmd>lua require("syntax-tree-surfer").surf("next", "visual", true)<CR>', mode = 'x', noremap = true, silent = true},
  {"H", '<Cmd>lua require("syntax-tree-surfer").surf("prev", "visual", true)<CR>', mode = 'x', noremap = true, silent = true},
  },
  wkeys = {
    {'<leader>r', group = 'refactor'},
    {'<leader>rl', '<Cmd>lua require("syntax-tree-surfer").move("n", false)<CR>', desc = 'move down'},
    {'<leader>rh', '<Cmd>lua require("syntax-tree-surfer").move("n", true)<CR>', desc = 'move up'},
    -- .select() will show you what you will be swapping with .move(), you'll get used to .select() and .move() behavior quite soon!
    {'<leader>rx', '<Cmd>lua require("syntax-tree-surfer").select()<CR>', desc = 'select'},
    -- .select_current_node() will select the current node at your cursor
    {'<leader>rc', '<Cmd>lua require("syntax-tree-surfer").select_current_node()<CR>', desc = 'select current'},
  },
}
