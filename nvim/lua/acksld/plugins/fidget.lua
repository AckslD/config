return {
  'j-hui/fidget.nvim',
  tag = 'legacy',
  config = {
    sources = {
      pyright = {
        ignore = true,
      },
    },
  },
}
