local dir = vim.fn.expand('~/todo/notes')

return {
  "epwalsh/obsidian.nvim",
  version = '*',
  lazy = true,
  event = {
    string.format("BufReadPre %s/**.md", dir),
    string.format("BufNewFile %s/**.md", dir),
  },
  cmd = {
    'ObsidianNew',
    'ObsidianOpen',
    'ObsidianToday',
    'ObsidianYesterday',
    'ObsidianTomorrow',
    'ObsidianQuickSwitch',
  },
  wkeys = {
    {'<leader>a', group = 'obsidian'},
    {'<leader>an', '<Cmd>ObsidianNew<CR>', desc = 'new'},
    {'<leader>ao', '<Cmd>ObsidianOpen<CR>', desc = 'open'},
    {'<leader>at', '<Cmd>ObsidianToday<CR>', desc = 'today'},
    {'<leader>aT', '<Cmd>ObsidianTomorrow<CR>', desc = 'tomorrow'},
    {'<leader>ay', '<Cmd>ObsidianYesterday<CR>', desc = 'yesterday'},
    {'<leader>af', '<Cmd>ObsidianQuickSwitch<CR>', desc = 'quick switch'},
  },
  dependencies = {
    "nvim-lua/plenary.nvim",
  },
  opts = {
    dir = dir,
    daily_notes = {
      folder = 'dailies',
      date_format = "%Y_%m_%d"
    },
    completion = {
      nvim_cmp = true,
      min_chars = 0,
    },
    note_id_func = function(title)
      local base = os.date('%Y_%m_%d')
      if title == nil then
        title = os.date('%H%M%S')
      else
        title = title:gsub(" ", "-"):gsub("[^A-Za-z0-9-]", ""):lower()
      end
      return string.format('%s_%s', base, title)
    end,
  },
}
