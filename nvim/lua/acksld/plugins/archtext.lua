return {
  "vigoux/architext.nvim",
  lazy = true,
  cmd = {'ArchitextREPL', 'Architext', 'A'},
}
