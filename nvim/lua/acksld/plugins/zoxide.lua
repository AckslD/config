return {
  'nanotee/zoxide.vim',
  cmd = {'Z', 'Lz'},
  dependencies = {'telescope-zoxide'},
  wkeys = {
    {'<leader>n', group = 'navigation'},
    {'<leader>nz', '<Cmd>lua require("telescope").extensions.zoxide.list()<CR>', desc = 'z list'},
  },
}
