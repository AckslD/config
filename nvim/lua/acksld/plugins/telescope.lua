return {
  'nvim-telescope/telescope.nvim',
  dependencies = {
    'popup.nvim',
    'plenary.nvim',
    'telescope-symbols.nvim',
    'telescope-file-browser.nvim',
    'telescope-fzf-native.nvim',
  },
  cmd = 'Telescope',
  keys = {
    {'<C-p>', '<Cmd>Telescope find_files find_command=fd,--hidden,--type,f,--exclude,.git<CR>'},
    {'<C-f>', '<Cmd>Telescope live_grep<CR>'},
  },
  wkeys = {
    {'<leader>f', group = 'find'},
    {'<leader>ff', '<Cmd>lua require("telescope.builtin").builtin({include_extensions = true})<CR>', desc = 'telescope'},
    {'<leader>fb', '<Cmd>Telescope buffers<CR>', desc = 'buffers'},
    {'<leader>fh', '<Cmd>Telescope help_tags<CR>', desc = 'help tags'},
    {'<leader>fk', '<Cmd>Telescope keymaps<CR>', desc = 'keymaps'},
    {'<leader>fo', '<Cmd>Telescope oldfiles<CR>', desc = 'old files'},
    {'<leader>fs', '<Cmd>Telescope current_buffer_fuzzy_find<CR>', desc = 'current buffer'},
    {'<leader>fe', '<Cmd>Telescope symbols<CR>', desc = 'symbols'},
    {'<leader>fr', '<Cmd>Telescope resume<CR>', desc = 'resume'},
    {'<leader>fn', '<Cmd>lua require("telescope").extensions.file_browser.file_browser()<CR>', desc = 'file browser'},

    {'<leader>fc', group = 'commands'},
    {'<leader>fcc', '<Cmd>Telescope commands<CR>', desc = 'commands'},
    {'<leader>fch', '<Cmd>Telescope command_history<CR>', desc = 'history'},

    {'<leader>fq', '<Cmd>Telescope quickfix<CR>', desc = 'quickfix'},

    {'<leader>fg', group = 'git'},
    {'<leader>fgg', '<Cmd>Telescope git_commits<CR>', desc = 'commits'},
    {'<leader>fgc', '<Cmd>Telescope git_bcommits<CR>', desc = 'bcommits'},
    {'<leader>fgb', '<Cmd>Telescope git_branches<CR>', desc = 'branches'},
    {'<leader>fgs', '<Cmd>Telescope git_status<CR>', desc = 'status'},

    {'<leader>l', group = 'lsp'},
    {'<leader>le', '<Cmd>Telescope diagnostics bufnr=0<CR>', desc = 'lsp errors'},
    {'<leader>lw', '<Cmd>Telescope lsp_dynamic_workspace_symbols<CR>', desc = 'workspace symbols'},
  },
  -- TODO extension fzf
  opts = {
    defaults = {
      dynamic_preview_title = true,
      vimgrep_arguments = {
        "rg",
        "--color=never",
        "--no-heading",
        "--with-filename",
        "--line-number",
        "--column",
        "--smart-case",
        "--hidden",
        "-g","!.git/**",
      },
    },
  },
}
