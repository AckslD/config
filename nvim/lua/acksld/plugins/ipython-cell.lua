return {
  'hanschen/vim-ipython-cell',
  cmd = {
    'IPythonCellExecuteCell',
    'IPythonCellExecuteCellJump',
    'IPythonCellExecuteCellVerbose',
    'IPythonCellExecuteCellVerboseJump',
    'IPythonCellRun',
    'IPythonCellRunTime',
    'IPythonCellClear',
    'IPythonCellClose',
    'IPythonCellPrevCell',
    'IPythonCellNextCell',
    'IPythonCellPrevCommand',
    'IPythonCellRestart',
    'IPythonCellInsertAbove',
    'IPythonCellInsertBelow',
    'IPythonCellToMarkdown',
  },
}
