local spec = function(filetype, name)
  return function()
    return require(string.format('formatter.filetypes.%s', filetype))[name]()
  end
end

return {
  'mhartington/formatter.nvim',
  cmd = 'Format',
  init = function()
    WK({
      {'<leader>lf',  '<Cmd>Format<CR>', desc = 'format' },
      {'<leader>lF',  '<Cmd>FormatWrite<CR>', desc = 'format write' },
      {
        mode = 'x',
        {'<leader>lf',  '<Cmd>Format<CR>', desc = 'format' },
      },
    })
  end,
  opts = {
    logging = true,
    log_level = vim.log.levels.WARN,
    filetype = {
      lua = { spec('lua', 'stylua') },
      json = { spec('json', 'jq') },
      html = { spec('html', 'prettier') },
      python = {
        function()
          return {
            exe = 'autopep8',
            args = { '-', '--experimental' },
            stdin = 1,
          }
        end,
        -- function()
        --   return {
        --     exe = 'isort',
        --     args = { '-', '-d', '--sp', '~/.isort.cfg' },
        --     stdin = 1,
        --   }
        -- end,
        -- spec('python', 'autopep8'),
        -- spec('python', 'isort'),
      },
      ['*'] = { spec('any', 'remove_trailing_whitespace') },
    },
  },
}
