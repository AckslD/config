local set_winbar = function(winbar)
  if vim.fn.has("nvim-0.8.0") ~= 1 then
    return
  end
  if vim.fn.winheight(0) <= 1 then
    return
  end
  local success, result = pcall(function()
    vim.wo.winbar = winbar
  end)
  -- if not success then
  --   vim.notify('couldn\'t set winbar since: '..result, vim.log.levels.WARN)
  -- end
end

local active_win = function()
  if vim.bo.buftype == 'nofile' then
    return
  end
  vim.wo.winhighlight=''
  set_winbar('%#LineNrAbove#%=%m %{getcwd()}/%#LineNr#%f %#MyBufNr#(%n)')
end

local non_active_win = function()
  if vim.bo.buftype == 'nofile' then
    return
  end
  vim.wo.winhighlight='LineNr:Comment'
  set_winbar('%#LineNrAbove#%=%m %{getcwd()}/%f (%n)')
end

R("funcs.autocmds").create_grouped_autocmds({
  MyNoInsertComments = {
    FileType = {
      -- don't insert comments
      command = 'setlocal formatoptions-=c formatoptions-=r formatoptions-=o',
    },
  },
  MyFTPlugin = {
    FileType = {
      --ftplugin
      callback = function() R("ftplugin").init() end,
    },
  },
  MyTermSetting = {
    TermOpen = {
      -- automatically enter insert mode on new terminals
      callback = function(opts)
        if opts.file:match('dap%-terminal') then
          return
        end
        vim.cmd('startinsert')
        vim.cmd('setlocal nonu')
      end,
    },
  },
  MyPersistentTerm = {
    TermClose = {
      -- prevent term buffer from accidentally closing
      callback = function()
        vim.cmd('stopinsert')
        vim.api.nvim_create_autocmd('TermEnter', {
          command = 'stopinsert',
          buffer = 0,
        })
      end,
      nested = true,
    },
  },
  MyLastPosition = {
    BufReadPost = {
      -- return to last edit position when opening files
      command = [[if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif]],
    },
  },
  MyUpdateConfigFiles = {
    BufWritePost = {
      -- update files in ~/.config directly when saving corresponding ones in ~/dev/config
      callback = function() R('funcs.config').save() end,
      -- NOTE ~/ does not work yet
      pattern = vim.fn.expand('~') .. '/dev/config/*',
    },
  },
  MyYankHighlight = {
    TextYankPost = {
        callback = function() vim.highlight.on_yank({higroup = "IncSearch", timeout = 150, on_visual = true}) end,
    },
  },
  MySearchWrap = {
    -- use wrap scan only for first search
    CmdlineEnter = {
      callback = function() vim.opt.wrapscan = true end,
    },
    CmdlineLeave = {
      callback = function() vim.fn.timer_start(1, function() vim.opt.wrapscan = false end) end,
    },
  },
  MyOpenSitePackageFile = {
    -- notify when opening site package python files instead of repo
    BufReadPost = {
      callback = function() vim.notify('Note that this is a file in */site-packages/*', vim.log.levels.WARN) end,
      pattern = '*/site-packages/*',
    },
  },
  MyActiveWin = {
    -- TODO are all these needed?
    VimEnter = {callback = active_win},
    BufWinEnter = {callback = active_win},
    WinEnter = {callback = active_win},
    WinNew = {callback = active_win},
    WinLeave = {callback = non_active_win},
    TabNewEntered = {callback = active_win},
  },
  MyDirHistory = {
    DirChanged = {
      callback = function()
        R('funcs.dir_history').on_dir_changed()
      end,
    },
  },
  MySetColorscheme = {
    VimEnter = {
      callback = function()
        -- R('colors.colorscheme').set('gruvbox')
        R('colors.colorscheme').set('nightfox', 'terafox')
        -- R('colors.colorscheme').set('tokyonight')
        -- TODO not sure why this is not otherwise called here
        vim.api.nvim_exec_autocmds('ColorScheme', {})
        -- R('colors.colorscheme').post_setup('terafox')
      end
    },
  },
  MyColorschemePost = {
    ColorScheme = {
      callback = function(opts)
        R('colors.colorscheme').post_setup(opts.match)
      end
    },
  },
  MyCloseFidget = {
    VimLeavePre = {
      command = [[silent! FidgetClose]],
    },
  },
  MyClearJumps = {
    VimEnter = {
      callback = function()
        vim.cmd.clearjumps()
      end
    },
  },
})

-- R('funcs.axelint').setup({
--   checks = {
--     python = function() return R('funcs.axelint.python') end,
--   },
-- })

R('funcs.skeletons').setup({
  ['pyrightconfig.json'] = 'pyrightconfig.json',
  ['*.tex'] = 'skeleton.tex',
})
