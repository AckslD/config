return function()
  WK({
    buffer = 0,
    {'<localleader>c', '<Cmd>Neorg gtd capture<CR>', desc = 'capture'},
    {'<localleader>e', '<Cmd>Neorg gtd edit<CR>', desc = 'edit'},
    {'<localleader>v', '<Cmd>Neorg gtd views<CR>', desc = 'views'},
    {'<localleader>n', '<Cmd>Neorg news all<CR>', desc = 'news'},
    {'<localleader>p', '<Cmd>Neorg presenter start<CR>', desc = 'start'},
    {'<localleader>P', '<Cmd>Neorg presenter close<CR>', desc = 'close'},
    {'<localleader>w', '<Cmd>Neorg workspaces<CR>', desc = 'workspaces'},
  })
end
