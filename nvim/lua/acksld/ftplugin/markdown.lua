return function()
    R("funcs.commands").define_commands({
        InsertImage = {
            command = function() R('funcs.ft.markdown').insert_image() end,
            buffer = 0,
        }
    })
end
