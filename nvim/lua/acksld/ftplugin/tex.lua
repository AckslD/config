return function()
    R('funcs.dedterm').setup{
        run = {
            cmd = "pdflatex &",
        },
    }
    local keymap = {
        o = {
            ':lua R("funcs.ft.tex").open()<CR><CR>',
            'open',
        },
        c = {
            ':lua R("funcs.ft.tex").clean()<CR><CR>',
            'clean',
        },
        R = {
            ':lua R("funcs.ft.tex").bib_compile()<CR>',
            'bib compile',
        },
    }

    IfHas("which-key", function(whichkey) whichkey.register(keymap, {prefix = '<localleader>', buffer = 0}) end)

    R("funcs.commands").define_commands({
        LatexInstallPackages = {
            command = function() R('funcs.ft.tex').install_packages() end,
            buffer = 0,
        }
    })
end
