return function()
  WK({
    buffer = 0,
    {'<localleader>r', '<Cmd>source %<CR>', desc = 'source file'},
    {'<localleader>t', '<Plug>PlenaryTestFile', desc = 'test file'},
  })
end
