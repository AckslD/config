local function py(func)
  return function(opts)
    return R("funcs.ft.python")[func](opts)
  end
end

return function()
    -- run and test
    R('funcs.dedterm').setup{
        run = {
            cmd = "p",
            post_args = "%:p"
        },
        test = {
            cmd = "p -m pytest --tb=short",
            post_args = "%:p",
            after = function()
              WK({
                buffer = 0,
                {'<localleader>d', function()
                  vim.cmd("cgetbuffer")
                  vim.cmd.normal('gg')
                  vim.fn.search('^_')
                  vim.cmd.PytrizeJump()
                  -- center cursor
                  -- XXX hangs nvim
                  -- vim.cmd.normal('zz')
                  vim.cmd.normal('mm')
                  vim.cmd.normal('G')
                  vim.cmd.normal('`m')
                  vim.cmd('copen')
                end, desc = 'debug first'},
              })
              vim.o.errorformat = ''..
                  [[%E%f:%l:\ in\ %o,]]..
                  [[%C\ %\{4}%m,]]..
                  [[%+G_%\+ %.%#,]]..
                  [[%+GDuring handling of the above exception\, another exception occurred:,]]..
                  [[%+GE\ %.%#,]]..
                  [[%Z%\S%.%#,]]..
                  [[%-C%.%#,]]..
                  [[%-G%.%#]]
            end,
        },
    }

    -- run and test using dap to debug
    vim.api.nvim_create_user_command('RunModule', py('run_module'), {nargs = 0})
    vim.api.nvim_create_user_command('RunDebug', py('run_debug'), {nargs = '*'})
    vim.api.nvim_create_user_command('TestDebug', py('test_debug'), {nargs = '*'})

    WK({
      buffer = 0,
      {
        '<localleader>j',
        function()
          R("funcs.terminal").dedicated(
            "p -m jupyter notebook %",
            {termname = 'jupyter', precmd = "tabnew"}
          )
        end,
        desc = 'open jupyter',
      },
      {'<localleader>f', py('test_function'), desc = 'test function'},
      {'<localleader>p', py('test_cwd'), desc = 'test cwd'},
      {'<localleader>R', py('run_debug'), desc = 'run (dap)'},
      {'<localleader>T', py('test_debug'), desc = 'test (dap)'},
      {'<localleader>F', py('test_function_debug'), desc = 'test function (dap)'},
      {'<localleader>P', py('test_cwd_debug'), desc = 'test cwd (dap)'},
      {'<localleader>s', py('toggle_f_string'), desc = 'toggle f-string'},
      {'<localleader>i', py('split_from_import'), desc = 'split from import'},
      {'<localleader>u', py('remove_unused_imports'), desc = 'remove unused imports'},
      {'<localleader>k', py('func_kwarg_to_dict_entry'), desc = 'kwarg -> key'},
      {'<localleader>K', py('dict_entry_to_func_kwarg'), desc = 'key -> kwarg'},
      {'<localleader>q', function()
        R('funcs.pytest').set_qf_list()
        vim.cmd('cfirst')
        vim.cmd('copen')
      end, desc = 'local qf list'},
      {'<localleader>m', py('ignore_mypy'), desc = 'ignore mypy'},
      {'<localleader>M', py('remove_ignore_mypy'), desc = 'remove ignore mypy'},
      {'<localleader>la', function()
        R('funcs.keep').cursor_wrap(function()
          vim.cmd('%! autopep8 -')
        end)()
      end, desc = 'autopep8'},
      {'<localleader>li', function()
        R('funcs.keep').cursor_wrap(function()
          vim.cmd('%! isort - -d --sp ~/.isort.cfg')
        end)()
      end, desc = 'isort'},
      {
        mode = 'v', buffer = 0,
        -- TODO fix how to access the range
        {'<localleader>k', py('func_kwarg_to_dict_entry'), desc = 'kwarg -> key'},
        {'<localleader>K', py('dict_entry_to_func_kwarg'), desc = 'key -> kwarg'},
      },
    })
    IfHas('plugin_settings.ipy', function(ipy) ipy.setup() end)

    vim.bo.formatprg=[[autopep8 - | isort -d -]]
    -- vim.opt_local.indentkeys:remove({'<:>'})

    vim.bo.makeprg = [[python\ %]]
    vim.bo.errorformat = ''..
        [[%*\sFile "%f"\, line %l\, %m,]]..
        [[%*\sFile "%f"\, line %l,]]..
        [[%f:%l: %m,]]..
        [[%f:%l:,]]..
        [[%-G%.%#]]
end
