local M = {}

M.palette = nil

M.set = function(colorscheme, subscheme)
  if colorscheme == 'gruvbox' then
    subscheme = subscheme or 'dark'
    local success, gruvbox = pcall(require, 'gruvbox.palette')
    if not success then
      return
    end
    if subscheme == 'dark' then
      M.palette = {
        red = gruvbox.bright_red,
        yellow = gruvbox.bright_yellow,
        blue = gruvbox.bright_blue,
        fg1 = gruvbox.light1,
        fg2 = gruvbox.fg2,
        bg2 = gruvbox.dark2,
        bg3 = gruvbox.dark3,
        bg4 = gruvbox.dark4,
      }
    else
      M.palette = {
        red = gruvbox.faded_red,
        yellow = gruvbox.faded_yellow,
        blue = gruvbox.faded_blue,
        fg1 = gruvbox.dark1,
        fg2 = gruvbox.dark2,
        bg2 = gruvbox.light2,
        bg3 = gruvbox.light3,
        bg4 = gruvbox.light4,
      }
    end
    vim.opt.background = subscheme
  elseif colorscheme == 'nightfox' then
    subscheme = subscheme or 'nightfox'
    colorscheme = subscheme
    local success, nightfox = pcall(require, 'nightfox.spec')
    if not success then
      return
    end
    nightfox = nightfox.load(subscheme).palette
    M.palette = {
      red = nightfox.red(),
      yellow = nightfox.yellow(),
      blue = nightfox.blue(),
      fg1 = nightfox.fg1,
      fg2 = nightfox.fg2,
      fg3 = nightfox.fg3,
      bg2 = nightfox.bg2,
      bg3 = nightfox.bg3,
      bg4 = nightfox.bg4,
    }
  elseif colorscheme == 'tokyonight' then
    subscheme = subscheme or 'storm'
    colorscheme = string.format('%s-%s', colorscheme, subscheme)
    local success, tokyonight = pcall(require, 'tokyonight')
    if not success then
      return
    end
    tokyonight.setup({
      -- Make telescope "borderless"
      on_highlights = function(hl, c)
        local prompt = "#2d3149"
        hl.TelescopeNormal = {
          bg = c.bg_dark,
          fg = c.fg_dark,
        }
        hl.TelescopeBorder = {
          bg = c.bg_dark,
          fg = c.bg_dark,
        }
        hl.TelescopePromptNormal = {
          bg = prompt,
        }
        hl.TelescopePromptBorder = {
          bg = prompt,
          fg = prompt,
        }
        hl.TelescopePromptTitle = {
          bg = prompt,
          fg = prompt,
        }
        hl.TelescopePreviewTitle = {
          bg = c.bg_dark,
          fg = c.bg_dark,
        }
        hl.TelescopeResultsTitle = {
          bg = c.bg_dark,
          fg = c.bg_dark,
        }
      end,
    })
    local tokyonight = require('tokyonight.colors').setup({style=subscheme})
    -- local util = require('tokyonight.util')
    M.palette = {
      red = tokyonight.red,
      yellow = tokyonight.yellow,
      blue = tokyonight.blue,
      fg1 = tokyonight.fg,
      fg2 = tokyonight.fg_dark,
      fg3 = tokyonight.comment,
      fg4 = tokyonight.fg_gutter,
      bg1 = tokyonight.bg,
      bg2 = tokyonight.bg_highlight,
      bg3 = tokyonight.fg_gutter,
      -- bg4 = util.lighten(tokyonight.comment, 0.01)
      bg4 = tokyonight.comment
    }
  else
    vim.notify('Unknown colorscheme: '..colorscheme, vim.log.levels.WARN)
    return
  end
  vim.cmd(string.format("colorscheme %s", colorscheme))
end

M.pick = function()
  vim.ui.select({
    {scheme = 'gruvbox', subscheme = 'dark'},
    {scheme = 'gruvbox', subscheme = 'light'},
    {scheme = 'nightfox', subscheme = 'nightfox'},
    {scheme = 'nightfox', subscheme = 'dayfox'},
    {scheme = 'nightfox', subscheme = 'dawnfox'},
    {scheme = 'nightfox', subscheme = 'duskfox'},
    {scheme = 'nightfox', subscheme = 'nordfox'},
    {scheme = 'nightfox', subscheme = 'terafox'},
    {scheme = 'nightfox', subscheme = 'carbonfox'},
    {scheme = 'tokyonight', subscheme = 'storm'},
    {scheme = 'tokyonight', subscheme = 'night'},
    {scheme = 'tokyonight', subscheme = 'day'},
  }, {
    prompt = 'Select colorscheme',
    format_item = function(item)
      if item.subscheme then
        return string.format('%s (%s)', item.scheme, item.subscheme)
      else
        return item.scheme
      end
    end
  }, function(item)
    if item then
      M.set(item.scheme, item.subscheme)
    end
  end)
end

M.post_setup = function(colorscheme)
  if not M.palette then
    return
  end
  local hl = R('funcs.highlight')

  local guibg
  local transparency
  if colorscheme == 'gruvbox' then
    guibg = 'none'
    transparency = 0.9
  else
    hl.update('WinSeparator', {fg = M.palette.blue, bg='none', bold = true})
    pcall(hl.update, 'TreeSitterContext', {bg=M.palette.bg3})
    transparency = 0.96
  end
  if vim.g.neovide then
    R('neovide').set_transparency(transparency)
  end
  hl.update('Normal', {fg = M.palette.fg1, bg=guibg})
  hl.update('NonText', {fg = M.palette.bg4, bg=guibg})
  hl.update('LineNr', {fg = M.palette.yellow, bg=guibg})
  hl.inherit('LineNrAbove', 'Comment')
  hl.inherit('LineNrBelow', 'Comment')
  hl.update('Comment', {italic = true})
  hl.inherit('MyBufNr', 'Error', {bold = true, italic = true})
  -- highlight.bright_comments()
  hl.link('TSError', 'Normal')

  if guibg then
    for _, name in ipairs({
      'TablineFill',
      'TablineSel',
      'Folded',
      'SignColumn',
    }) do
      hl.update(name, {bg=guibg})
    end
  end
end

return M
