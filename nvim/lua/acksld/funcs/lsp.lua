vim.lsp.inlay_hint.toggle = function()
  vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())
end

return {
  get_on_attach = function(lsp)
    return function(client, bufnr) -- {{{
      local opts = { noremap=true, silent=true }

      local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
      local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

      buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
      if client.server_capabilities.documentSymbolProvider then
        IfHas('nvim-navbuddy', function(navbuddy)
          navbuddy.attach(client, bufnr)
        end)
      end
      -- if client.server_capabilities.inlayHintProvider then
      --   vim.lsp.inlay_hint.enable(true)
      -- end
      IfHas('lsp_signature', function(lsp_signature)
        lsp_signature.on_attach({
            bind = true, -- This is mandatory, otherwise border config won't get registered.
            floating_window = false, -- show hint in a floating window, set to false for virtual text only mode
          })
      end)

      -- Mappings. {{{
      local telescope_or_builtin = function(name, telescope_name)
        if not telescope_name then
          telescope_name = string.format('lsp_%s', name)
        end
        local success, telescope_builtin = pcall(require, 'telescope.builtin')
        if success then
          telescope_builtin[telescope_name]({on_no_result = function()
            vim.notify(string.format("couldn't find %s", name))
          end})
        else
          vim.lsp.buf[name]()
        end
      end
      WK({
        {
          buffer = bufnr,
          {'<leader>l', group = "lsp"},
          {'<leader>lu', '<Cmd>LspStart<CR>', desc = 'start'},
          {'<leader>li', function() telescope_or_builtin('implementation', 'lsp_implementations') end, desc = 'implementation'},
          {'<leader>lt', function() telescope_or_builtin('type_definition', 'lsp_type_definitions') end, desc = 'type definition'},
          {'<leader>lh', function() telescope_or_builtin('references') end, desc = 'references'},
          {'<leader>ls', function() telescope_or_builtin('incoming_calls') end, desc = 'incoming calls'},
          {'<leader>lS', function() telescope_or_builtin('outgoing_calls') end, desc = 'outcoming calls'},
          {'<leader>lo', function() telescope_or_builtin('document_symbol', 'lsp_document_symbols') end, desc = 'symbols'},
          -- workspace
          {'<leader>la', '<Cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', desc = 'add workspace'},
          {'<leader>lR', '<Cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', desc = 'remove workspace'},
          {'<leader>ll', '<Cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', desc = 'list workspace'},
          -- code action
          {'<leader>lq', "<Cmd>lua vim.lsp.buf.code_action()<CR>", desc = 'code action'},

          {'<leader>r', group = "refactor"},
          -- rename
          {'<leader>rn', "<Cmd>lua vim.lsp.buf.rename()<CR>", desc = 'rename (lsp)'},

          -- preview definition
          {'gd', function() telescope_or_builtin('definition', 'lsp_definitions') end, desc = 'lsp definition'},
          {'gD', "<Cmd>lua vim.lsp.buf.declaration()<CR>", desc = 'lsp declaration'},
        },
        {
          mode = 'v', buffer = bufnr,
          {'<leader>l', group = "lsp"},
          {'<leader>lq', "<Cmd>'<,'>lua vim.lsp.buf.range_code_action()<CR>", desc = 'code action'},
        },
      })
      -- Set some keybinds conditional on server capabilities
      if client.server_capabilities.documentFormattingProvider then
        WK({
          {'<leader>lf', '<Cmd>lua vim.lsp.buf.format({async = false})<CR>', desc = 'format', buffer = bufnr},
        })
      end

      -- show hover doc
      buf_set_keymap('n', 'K', "<Cmd>lua vim.lsp.buf.hover()<CR>", opts)
      vim.keymap.set('i', '<C-s>', vim.lsp.buf.signature_help, {desc = 'signature help'})
      -- }}}

      -- Set autocommands conditional on server_capabilities
      if client.server_capabilities.documentHighlightProvider then
        R('funcs.autocmds').create_grouped_autocmds({
          MyLSPHighlightClear = {
            CursorMoved = {
              callback = vim.lsp.buf.clear_references,
              buffer = 0,
            },
          },
        })
      end
      local jedi_capabilities = {
        -- hoverProvider = true,
        -- completionProvider = true,
      }
      if client.name == 'pyright' then
        for jedi_capability, _ in pairs(jedi_capabilities) do
          client.server_capabilities[jedi_capability] = false
        end
      end
      if client.name == 'jedi_language_server' then
        for capability, _ in pairs(client.server_capabilities) do
          if capability:find('%w*Provider') and not jedi_capabilities[capability] then
            client.server_capabilities[capability] = false
          end
        end
      end
      if client.name == 'pylyzer' then
        for capability, _ in pairs(client.server_capabilities) do
          if capability:find('%w*Provider') then
            client.server_capabilities[capability] = false
          end
        end
      end
      if lsp == 'efm' and vim.o.filetype == 'python' then
        client.resolved_capabilities.document_formatting = false
        client.resolved_capabilities.document_range_formatting = false
      end
    end -- }}}
  end,
}
