local M = {}

-- diagnostics
M.format = function(diagnostic)
  if (
    diagnostic.source ~= "Pyright"
    and diagnostic.user_data
    and diagnostic.user_data.lsp
    and diagnostic.user_data.lsp.code
  ) then
    -- TODO could also do with suffix
    return string.format('%s %s', diagnostic.user_data.lsp.code, diagnostic.message)
  else
    return diagnostic.message
  end
end

vim.diagnostic.config({
  virtual_text = {
    source = true,
    format = M.format,
  },
  signs = true,
  float = {
    header = 'Diagnostics',
    source = true,
    border = 'rounded',
    format = M.format,
  },
})

local diagnostics_enabled = true

vim.diagnostic.toggle_all = function()
  if diagnostics_enabled then
    vim.diagnostic.disable()
    diagnostics_enabled = false
  else
    vim.diagnostic.enable()
    diagnostics_enabled = true
  end
end

return M
