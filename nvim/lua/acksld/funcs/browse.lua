local browse = {}

browse.browse = function(url)
    vim.notify('opened ' .. url)
    os.execute('qutebrowser '..url)
end

return browse
