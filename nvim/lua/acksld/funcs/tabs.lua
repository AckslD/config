local M = {}

local tabs = {
  cwds = {},
  -- names = {},
  numbers = {},
}

M.get_tab = function(tabnr)
  return tabs.numbers[tabnr]
end

-- local get_bufname = function(tabnr)
--   local buflist = vim.fn.tabpagebuflist(tabnr)
--   local winnr = vim.fn.tabpagewinnr(tabnr)
--   return vim.fn.bufname(buflist[winnr])
-- end

local make_tab_label = function(tabnr)
  -- if not vim.api.nvim_tabpage_is_valid(tabnr) then
  --   -- FIX this seems to give the wrong thing in some cases, new tabs?
  --   return
  -- end
  -- for _, winid in ipairs(vim.api.nvim_tabpage_list_wins(tabnr)) do
  --   local bufnr = vim.api.nvim_win_get_buf(winid)
  --   local ft = vim.api.nvim_buf_get_option(bufnr, 'filetype')
  --   if ft:find('%w*-term') then
  --     return ft
  --   end
  -- end
  if tabnr == 1 then
    tabs = {
      cwds = {},
      -- names = {},
      numbers = {},
    }
  end
  local cwd = vim.fn.getcwd(-1, tabnr)
  -- TODO warn about this?
  local hl = ''
  -- if tabs.cwds[cwd] and get_bufname(tabnr) ~= '' then
  -- TODO how to get working?
  --   hl = '%#DiagnosticVirtualError#%'
  --   -- vim.schedule(function()
  --   --   vim.notify(string.format('Using more than one tab for the same cwd: %s', cwd), vim.log.levels.WARN)
  --   -- end)
  -- end
  tabs.cwds[cwd] = tabnr
  local parts = vim.split(cwd, '/', {trimempty = true})
  local name = parts[#parts]
  tabs.numbers[tabnr] = {
    cwd = cwd,
    name = name,
  }
  -- distinguish cwds with the same basename?
  -- while tabs.names[name] and  do
  -- end
  -- tabs[name] = {
  --   cwds = cwd,
  --   tabnr = tabnr,
  -- }
  return hl .. name
end
-- _G.my_tab_label = function(tabnr)
--   -- return vim.fn.getcwd(winnr, tabnr)
--   P('n', tabnr)
--   local buflist = vim.fn.tabpagebuflist(tabnr)
--   if type(buflist) ~= 'table' then
--     return 'not a table'
--   end
--   P('buflist', buflist)
--   local winnr = vim.fn.tabpagewinnr(tabnr)
--   P('winnr', winnr)
--   local bufname = vim.fn.bufname(buflist[winnr])
--   P('bufname', bufname)
--   if bufname == '' then
--     return '[No Name]'
--   else
--     return bufname
--   end
-- end

M.tabline = function()
  _G.my_tab_label = make_tab_label
  local s = ''
  for i=1,vim.fn.tabpagenr('$') do
    -- select the highlighting
    if i == vim.fn.tabpagenr() then
      s = s..'%#TabLineSel#'
    else
      s = s..'%#TabLine#'
    end

    -- set the tab page number (for mouse clicks)
    s = s..'%' .. i .. 'T'

    -- the label is made by MyTabLabel()
    s = s..' %{v:lua.my_tab_label(' .. i .. ')} '
    -- s = s..' %{getcwd(-1, ' .. i .. ')} '
  end

  -- after the last tab fill with TabLineFill and reset tab page nr
  s = s..'%#TabLineFill#%T'

  -- right-align the label to close the current tab page
  if vim.fn.tabpagenr('$') > 1 then
    s = s..'%=%#TabLine#%999Xx'
  end

  return s
end

return M
