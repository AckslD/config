local M = {}

M.cursor_wrap = function(func)
  return function(...)
    local save_cursor = vim.fn.getpos(".")
    local output = func(...)
    vim.fn.setpos('.', save_cursor)
    return output
  end
end

M.regs_wrap = function(func, reg_names)
  return function(...)
    local reg_infos = {}
    for _, reg_name in ipairs(reg_names) do
      reg_infos[reg_name] = vim.fn.getreginfo(reg_name)
    end
    local output = func(...)
    for reg_name, reg_info in pairs(reg_infos) do
      vim.fn.setreg(reg_name, reg_info)
    end
    return output
  end
end

M.unnamed_wrap = function(func)
  return M.regs_wrap(func, {'"'})
end

M.marks_wrap = function(func, mark_names)
  return function(...)
    local mark_infos = {}
    for _, mark_name in ipairs(mark_names) do
      mark_infos[mark_name] = vim.fn.getpos(mark_name)
    end
    local output = func(...)
    for mark_name, mark_info in pairs(mark_infos) do
      vim.fn.setpos(mark_name, mark_info)
    end
    return output
  end
end

M.visual_wrap = function(func)
  return M.marks_wrap(func, {"'<", "'>"})
end

M.noautocmd_wrap = function(func, ignore)
  return function(...)
    local current_eventignore = vim.o.eventignore
    vim.o.eventignore = ignore
    local output = func(...)
    vim.o.eventignore = current_eventignore
    return output
  end
end

for name, wrap in pairs(M) do
  M[name:gsub('_wrap$', '')] = function(callback, ...)
    return wrap(callback, ...)()
  end
end

return M
