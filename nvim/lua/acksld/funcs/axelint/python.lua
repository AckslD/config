local ts = vim.treesitter
local parse_query
if ts.query.parse then
  parse_query = ts.query.parse
else
  parse_query = ts.parse_query
end

local lang = 'python'

local def_query = parse_query(lang, [[
  [
    (function_definition
      name: (identifier) @def-name
      body: (block
        (expression_statement
          (string) @doc-string
        )?
      )
    )
    (class_definition
      name: (identifier) @def-name
      body: (block
        (expression_statement
          (string) @doc-string
        )?
      )
    )
  ]
]])
local get_func_query = function(func_name)
  return parse_query(lang, string.format([[
    (_
      (identifier) @func-name (#eq? @func-name "%s")
    )
  ]], func_name))
end

local get_definitions_without_docstrings = function(root, bufnr)
  local func_nodes = {}
  for _, match, _ in def_query:iter_matches(root, bufnr) do
    if match[2] == nil then -- only if second capture (doc-string is missing)
      if match[1]:parent():parent():type() == 'module' then -- only module level for now
        table.insert(func_nodes, match[1])
      end
    end
  end
  return func_nodes
end

local is_func_used = function(root, bufnr, func_name)
  local query = get_func_query(func_name)
  for _, match, _ in query:iter_matches(root, bufnr) do
    if match[1]:parent():type() ~= 'function_definition' then
      return true
    end
  end
  return false
end

local unused_undocumented_functions = function(root, bufnr, diagnostics)
  local nodes = get_definitions_without_docstrings(root, bufnr)
  for _, node in ipairs(nodes) do
    local func_name = ts.get_node_text(node, bufnr)
    if not is_func_used(root, bufnr, func_name) then
      table.insert(diagnostics, {
        lnum = node:start(),
        message = 'unused definition without doc-string',
      })
    end
  end
end

local from_import_query = parse_query(lang, [[
  ((import_from_statement) @from-import)
]])

local multiple_from_imports = function(root, bufnr, diagnostics)
  for _, match, _ in from_import_query:iter_matches(root, bufnr) do
    for _, node in pairs(match) do
      if #node:field('name') > 1 then
        table.insert(diagnostics, {
          lnum = node:start(),
          message = 'multiple import using from',
        })
      end
    end
  end
end

return {
  unused_undocumented_functions,
  multiple_from_imports,
}
