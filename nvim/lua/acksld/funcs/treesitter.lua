local M = {}

local get_lang = function(bufnr, lang)
  if lang == nil then
    if bufnr == nil then
      return vim.bo.filetype
    else
      return vim.api.nvim_buf_get_option(bufnr, 'filetype')
    end
  else
    return lang
  end
end

M.disable_large_files = function()
  vim.treesitter.get_parser = (function(orig)
    return function(bufnr, lang, opts)
      if get_lang(bufnr, lang) == "json" and vim.api.nvim_buf_line_count(bufnr) > 1000 then
        error("lol treesitter go brrr")
      end
      return orig(bufnr, lang, opts)
    end
  end)(vim.treesitter.get_parser)
end

return M
