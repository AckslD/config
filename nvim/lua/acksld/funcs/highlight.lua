local M = {}
local palette = R('colors.colorscheme').palette

M.use_bright_comments = false

M.inherit = function(name, base, opts)
  local conf = vim.api.nvim_get_hl_by_name(base, {})
  opts = opts or {}
  for k, v in pairs(opts) do
    conf[k] = v
  end
  vim.api.nvim_set_hl(0, name, conf)
end

M.update = function(name, opts)
  M.inherit(name, name, opts)
end

M.link = function(name, target)
  vim.api.nvim_set_hl(0, name, {link = target})
end

M.discrete_comments = function()
  M.update('Comment', {fg = palette.fg2, italic = true})
  M.use_bright_comments = false
end

M.bright_comments = function()
  M.update('Comment', {fg = palette.red, bold = true, italic = true})
  M.use_bright_comments = true
end

M.toggle_bright_comments = function()
    if M.use_bright_comments then
        M.discrete_comments()
    else
        M.bright_comments()
    end
end

return M
