local M = {}

M.unload = function(pattern)
  for name, _ in pairs(package.loaded) do
    if string.find(name, pattern) then
      package.loaded[name] = nil
    end
  end
end

return M
