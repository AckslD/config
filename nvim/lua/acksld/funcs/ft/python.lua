local M = {}

local keep = R('funcs.keep')
local set_cursor = vim.api.nvim_win_set_cursor
local get_cursor = vim.api.nvim_win_get_cursor
local set_lines = vim.api.nvim_buf_set_lines
local get_lines = vim.api.nvim_buf_get_lines
local get_text = vim.api.nvim_buf_get_text
local get_current_line = vim.api.nvim_get_current_line
local set_current_line = vim.api.nvim_set_current_line

local replace_termcodes = function(str)
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local normal = function(str, opts)
    if opts == nil then
        opts = {noremap = true}
    end
    local flag = ''
    if opts.noremap == true then
        flag = '!'
    end
    vim.cmd(replace_termcodes("normal"..flag.." "..str))
end

local getcurchar = function()
  local row, col = unpack(get_cursor(0))
  return get_text(0, row - 1, col, row - 1, col + 1, {})[1]
end

-- NOTE Below functions taken from https://github.com/alfredodeza/pytest.vim
-- Always goes back to the first instance
-- and returns that if found
local find_python_object = function(obj)
    local objregexp
    local max_indent_allowed
    local orig_line = vim.fn.line('.')
    local orig_indent = vim.fn.indent(orig_line)


    if (obj == "class") then
        objregexp  = [[\v^\s*(.*class)\s+(\w+)\s*]]
        max_indent_allowed = 0
    elseif (obj == "method") then
        objregexp = [[\v^\s*(.*def)\s+(\w+)\s*\(\_s*(self[^)]*)]]
        max_indent_allowed = 4
    elseif (obj == "function") then
        objregexp = [[\v^\s*(.*def)\s+(\w+)\s*\(\_s*(.*self)@!]]
        max_indent_allowed = orig_indent
    else
        vim.cmd('echoerr "Unknown object: "'..obj)
        return false
    end

    local flag = "Wb"

    while (vim.fn.search(objregexp, flag) > 0) do
        --
        -- Very naive, but if the indent is less than or equal to four
        -- keep on going because we assume you are nesting.
        -- Do not count lines that are comments though.
        --
        local indent = vim.fn.indent(vim.fn.line('.'))
        if (indent <= 4) and (vim.api.nvim_eval([[getline(line('.')) =~ '\v^\s*#(.*)']]) == 0) then
          if (indent <= max_indent_allowed) then
            return true
        end
      end
    end
    return false
end

local name_of_current_function = keep.cursor_wrap(function()
    normal('$<CR>')
    local find_object = find_python_object('function')
    local object = nil
    if (find_object) then
        local line = vim.fn.getline('.')
        local match_result = vim.fn.matchlist(line, [[ *def \+\(\w\+\)]])
        object = match_result[2]
    end
    return object
end)

local name_of_current_class = keep.cursor_wrap(function()
    normal('$<CR>')
    local find_object = find_python_object('class')
    local object = nil
    if (find_object) then
        local line = vim.fn.getline('.')
        local match_result = vim.fn.matchlist(line, [[ *class \+\(\w\+\)]])
        object = match_result[2]
    end
    return object
end)


local name_of_current_method = keep.cursor_wrap(function()
    normal('$<CR>')
    local find_object = find_python_object('method')
    local object = nil
    if (find_object) then
        local line = vim.fn.getline('.')
        local match_result = vim.fn.matchlist(line, [[ *def \+\(\w\+\)]])
        object = match_result[2]
    end
    return object
end)

-- NOTE does not take into account pyenv
M.get_python_path = function()
    local venv = os.getenv("VIRTUAL_ENV")
    if venv then
        return string.format("%s/bin/python", venv)
    else
        return '/usr/bin/python'
    end
end

local function run_dap(opts)
  local success, dap = pcall(require, 'dap')
  if not success then
    vim.notify('Could not require dap: '..dap, vim.log.levels.WARN)
    return
  end
  dap.run({
      name = "Launch file",
      type = 'python',
      request = 'launch',
      program = opts.program,
      module = opts.module,
      pythonPath = 'python',
      console = 'integratedTerminal',
      args = opts.args,
  })
end

local function get_run_args(opts)
    if opts == nil then
        opts = {}
    end
    local args = opts.args or ''
    return vim.split(args, ' ')
end

M.run_debug = function(opts)
    run_dap({
        program = "${file}",
        args = get_run_args(opts),
    })
end

local function get_test_args(opts)
    if opts == nil then
        opts = {}
    end
    local args
    if opts.args then
        args = opts.args .. ' ${file}'
    else
        args = '${file}'
    end
    return vim.split(args, ' ', {trimempty = true})
end

M.test_debug = function(opts)
    run_dap({
        module = 'pytest',
        args = get_test_args(opts)
    })
end

local function get_test_id()
    local save_cursor = vim.fn.getcurpos()
    local current_function = name_of_current_function()
    vim.fn.setpos('.', save_cursor)
    if (current_function == nil) then
        vim.cmd('echoerr "Unable to find a matching function for testing"')
        return
    end
    local test_id = vim.fn.expand('%').."::"..current_function
    return test_id
end

M.test_function = function()
    normal('wa')
    local test_id = get_test_id()
    R('funcs.terminal').dedicated("p -m pytest -x "..test_id, {})
end

M.test_function_debug = function()
    normal('wa')
    local test_id = get_test_id()
    run_dap({
        module = 'pytest',
        args = {test_id},
    })
end

M.test_cwd = function()
    normal('wa')
    R('funcs.terminal').dedicated("p -m pytest -x "..vim.fn.getcwd(), {})
end

M.test_cwd_debug = function()
    run_dap({
        module = 'pytest',
        args = {vim.fn.getcwd()},
    })
end

M.run_module = function()
  local module = vim.fn.expand('%:r'):gsub('/', '.')
  print(module)
  R('funcs.terminal').dedicated("p -m " .. module, {})
end

local function shift_cursor_col(cursor, num_cols)
    cursor[3] = cursor[3] + num_cols
end

local function is_quote(char)
    return char == "'" or char == '"'
end

M.toggle_f_string = keep.visual_wrap(function()
    -- make sure textobj plugin is loaded
    -- TODO use treesitter
    local success, results = pcall(function()
      require('packer').loader('vim-textobj-quotes')
    end)
    if not success then
      vim.notify("Couldn't load vim-textobj-quotes since: "..results, vim.log.level.WARN)
      return
    end
    local save_cursor = vim.fn.getpos(".")
    -- find nearest string and go to the start
    normal("vaq", {noremap = false})  -- note we use to separate commands due to issue with whichkey
    normal("<Esc>`<", {noremap = true})
    -- check if string was found
    if not is_quote(getcurchar()) then
        return
    end
    -- go to char before the current string
    normal("h")
    while is_quote(getcurchar()) do
        normal("h")
    end
    -- toggle f before string
    if getcurchar() == 'f' then
        normal("x")
        shift_cursor_col(save_cursor, -1)
    else
        normal("af")
        shift_cursor_col(save_cursor, 1)
    end
    vim.fn.setpos('.', save_cursor)
end)

M.toggle_key_argument = function()
  local line = get_current_line()
end

M.split_from_import = keep.cursor_wrap(function()
  local indent = vim.fn.indent('.')
  local line = get_current_line()
  local package, modules = line:match('from (.*) import ([%w_, ]*)')
  if not package then
    return
  end
  local new_lines = {}
  for _, module in ipairs(vim.fn.split(modules, [[,\s*]])) do
    table.insert(new_lines, (' '):rep(indent) .. string.format('from %s import %s', package, module))
  end
  local row, _  = unpack(get_cursor(0))
  set_lines(0, row - 1, row, false, new_lines)
end)

local get_ns_id_by_name = function(name)
  for ns_id, ns in pairs(vim.diagnostic.get_namespaces()) do
    if ns.name == name then
      return ns_id
    end
  end
end

M.remove_unused_imports = function()
  local ns_id = get_ns_id_by_name('flake8')
  if ns_id == nil then
    return
  end
  local lines = {}
  for _, d in ipairs(vim.diagnostic.get(0, {namespace=ns_id})) do
    if d.code == "F401" then
      table.insert(lines, 1, d.lnum)  -- reverse lines
    end
  end
  for _, line in ipairs(lines) do
    set_lines(0, line, line + 1, false, {})
  end
end

local update_line = function(lnum, update)
  set_lines(0, lnum, lnum + 1, false, {
    update(get_lines(0, lnum, lnum + 1, false)[1])
  })
end

local get_current_lnum = function()
  return vim.api.nvim_win_get_cursor(0)[1] - 1
end

M.ignore_mypy = function()
  local ns_id = get_ns_id_by_name('mypy')
  if ns_id == nil then
    return
  end
  local lnum = get_current_lnum()
  local codes = {}
  for _, d in ipairs(vim.diagnostic.get(0, {namespace=ns_id, lnum=lnum})) do
    if d.code then
      codes[d.code] = true
    end
  end
  if codes then
    update_line(lnum, function(current)
      -- TODO could check if comment already exists maybe?
      return string.format("%s  # type: ignore[%s]", current, table.concat(vim.tbl_keys(codes), ','))
    end)
  end
end

M.remove_ignore_mypy = function()
  update_line(get_current_lnum(), function(current)
    local new, _ = current:gsub("%s*# type: ignore%[[^%[%]]*%]", "")
    return new
  end)
end

local func_kwarg_to_dict_entry = function()
  print('mode', vim.fn.mode())
  local current_line = vim.api.nvim_win_get_cursor(0)[1]
  print('current_line', current_line)
  local success = vim.fn.search([[\w\+=]], 'szc', current_line)
  if success == 0 then
    return
  end
  vim.cmd([[normal saiw'f=xi: ]])
  vim.cmd([[normal ``]])
end

M.func_kwarg_to_dict_entry = keep.cursor_wrap(function()
  local mode = vim.fn.mode()
  if mode == 'v' or mode == 'V' then
    local start = vim.api.nvim_win_get_cursor(0)[1]
    local end_ = vim.fn.line('v')
    if end_ < start then
      start, end_ = end_, start
    end
    vim.cmd([[normal \<Esc>]])
    print('mode', vim.fn.mode())
    for line = start, end_ do
      print('line', line)
      vim.api.nvim_win_set_cursor(0, {line, 0})
      func_kwarg_to_dict_entry()
    end
  end

  func_kwarg_to_dict_entry()
end)

M.dict_entry_to_func_kwarg = keep.cursor_wrap(function()
  local current_line = vim.api.nvim_win_get_cursor(0)[1]
  local success = vim.fn.search([['\w\+':]], 'szc', current_line)
  if success == 0 then
    return
  end
  vim.cmd([[normal sd'f:r=f x]])
  vim.cmd([[normal ``]])
end)

return M
