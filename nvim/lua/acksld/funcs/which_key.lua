local M = {}

M.add = function(...)
  local status, wk = pcall(require, "which-key")
  if not (status) then
    -- vim.notify("couldn't load which-key, skipping mappings")
    return
  end
  wk.add(...)
end

return M
