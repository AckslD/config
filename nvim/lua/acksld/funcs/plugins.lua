local M = {}

M.workon = function()
  local config = require('lazy.core.config')
  vim.ui.select(vim.tbl_values(config.plugins), {
    prompt = 'lcd to:',
    format_item = function(plugin)
      return string.format('%s (%s)', plugin.name, plugin.dir)
    end,
  }, function(plugin)
    if not plugin then
      return
    end
    vim.schedule(function()
      vim.cmd.lcd(plugin.dir)
    end)
  end)
end

M.find_file = function()
  require("telescope.builtin").find_files({
    cwd = require("lazy.core.config").options.root,
  })
end

M.find_line = function()
  require("telescope.builtin").live_grep({
    cwd = require("lazy.core.config").options.root,
  })
end

return M
