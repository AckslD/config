local M = {}

M.save = function()
    local current = vim.api.nvim_buf_get_name(0)
    local corresponding = current:gsub('/dev/config/', '/.config/')
    vim.cmd('write! ' .. corresponding)
end

M.source = function()
    UnloadModules()
    vim.cmd("source ~/.config/nvim/init.lua")
    vim.notify('Config reloaded 🎉')
end

M.compile = function()
    M.source()
    R('plugins')()
    IfHas('packer', function(packer) packer.compile() end)
end

return M
