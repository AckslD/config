local M = {}

M.start = function (prof_file)
  vim.cmd("profile start "..prof_file)
  vim.cmd("profile func *")
  vim.cmd("profile file *")
end

M.stop = function()
  vim.cmd("profile stop")
end

M.profile_func = function(func, filename)
  local prof = require("profile")
  prof.start("*")
  func()
  prof.stop()
  filename = filename or "profile.json"
  vim.notify(string.format("Writing %s...", filename))
  prof.export(filename)
end

M.profile_command = function(cmd, filename)
  M.profile_func(function() vim.cmd(cmd) end, filename)
end

local feedkeys = function(keys, mode)
  mode = mode or 'n'
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(keys, true, true, true), mode, true)
end


M.profile_keys = function(keys, filename)
  M.profile_func(function() feedkeys(keys) end, filename)
end

return M
