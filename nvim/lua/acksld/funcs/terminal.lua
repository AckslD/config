local terminal = {}

local last_command = {}

local function get_command(command, base_filetype, opts)
  local filetype = vim.bo.filetype
  if filetype ~= base_filetype then
    if last_command[base_filetype] == nil then
      vim.cmd(
      'echoerr "Something went wrong, didn\'t have a last command for base filetype: '..base_filetype..'"'
      )
    end
    return last_command[base_filetype]
  end
  if opts.args then
    command = command .. " " .. opts.args
  end
  if opts.post_args then
    command = command .. " " .. opts.post_args
  end
  local expanded_command = vim.fn.expandcmd(command)
  if not opts.no_time then
    expanded_command = string.format('time (%s)', expanded_command)
  end
  last_command[filetype] = expanded_command
  return expanded_command
end

local get_base_filetype = function()
  return vim.fn.substitute(vim.bo.filetype, '-term$', '', '')
end

-- Function for running dedicated terminal (thanks Sam MS for original idea :))
terminal.dedicated = function(command, opts)
  local base_filetype = get_base_filetype()
  local termname = opts.termname or base_filetype
  local bufname = string.format('dedterm-%s', termname)
  local expanded_command = get_command(command, base_filetype, opts)
  -- If buffer exists, delete it first
  if vim.fn.bufexists(bufname) > 0 then
    vim.cmd('bwipeout! '..bufname)
  end
  -- Create new buffer and run command
  vim.cmd(opts.precmd or 'tabnew')
  vim.cmd('terminal '..expanded_command)
  vim.cmd('normal G')
  local current_bufname = vim.fn.bufname()
  -- Remove name then rename to avoid the old name become unlisted
  vim.cmd('file '..bufname)
  -- Wipeout the old plain terminal (unlisted) buffer name after renaming
  vim.cmd('bwipeout '..current_bufname:gsub(' ', [[\ ]]))
  vim.bo.filetype = base_filetype..'-term'
end

return terminal
