#!/usr/bin/bash
date >> ~/log/redshift_hooks.txt
echo "$@" >> ~/log/redshift_hooks.txt
echo "END" >> ~/log/redshift_hooks.txt
case $1 in
  period-changed)
    case $3 in
      night)
        echo "set 30" >> ~/log/redshift_hooks.txt
        ~/.local/bin/brightness set 30 2>> ~/log/redshift_hooks.txt
        ;;
      daytime)
        echo "set 50" >> ~/log/redshift_hooks.txt
        ~/.local/bin/brightness set 50
        ;;
      *)
        echo "set 50" >> ~/log/redshift_hooks.txt
        ~/.local/bin/brightness set 50
    esac
esac
