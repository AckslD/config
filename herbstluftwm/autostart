#!/usr/bin/env bash

# this is a simple config for herbstluftwm

hc() {
    herbstclient "$@"
}

hc emit_hook reload

#xsetroot -solid '#5A8E3A'
#xsetroot -solid '#000000'

# kill existing polybars
pkill polybar

# remove all existing keybindings
hc keyunbind --all

# keybindings
# if you have a super key you will be much happier with Mod set to Mod4
#Mod=Mod1    # Use alt as the main modifier
Mod=Mod4   # Use the super key as the main modifier
TERM_EMULATOR=kitty
hc_virt_monitors=~/.local/bin/hc_virt_monitors

hc keybind $Mod-c hc reload
hc keybind $Mod-Shift-t spawn dump-hlwm-data

hc keybind $Mod-Shift-q spawn zsh -c "prompt-yn \"Shutdown computer?\" -c \"shutdown now\" -w"
hc keybind $Mod-Shift-r spawn zsh -c "prompt-yn \"Restart computer?\" -c \"shutdown -r now\" -w"
hc keybind $Mod-Shift-c close
hc keybind $Mod-Return spawn $TERM_EMULATOR
hc keybind $Mod-Shift-Return spawn neovide --nofork
hc keybind $Mod-t spawn $TERM_EMULATOR
hc keybind $Mod-Shift-i spawn qutebrowser
hc keybind Control-Shift-l spawn zsh -c "lockAndSleep --sleep"
hc keybind Control-Alt-l spawn zsh -c lockAndSleep
hc keybind $Mod-Shift-s spawn $TERM_EMULATOR -e spt
# hc keybind $Mod-m spawn $EMAIL_CLIENT
# hc keybind $Mod-Shift-m spawn $MESSAGE_CLIENT
hc keybind $Mod-e spawn rofi -show run
hc keybind $Mod-Shift-e spawn rofi -show drun
hc keybind $Mod-w spawn rofi -show window
hc keybind $Mod-n spawn zsh -c "wifiman -f -w"
hc keybind $Mod-Shift-x spawn $TERM_EMULATOR --title="vimxclip(no-float)" -e zsh -c "vimxclip"
hc keybind $Mod-o spawn $TERM_EMULATOR --title="noteman(no-float)" -e zsh -c "noteman"
hc keybind $Mod-Shift-o spawn $TERM_EMULATOR --title="vimnoteman(no-float)" -e zsh -c "vimnoteman"
hc keybind $Mod-d spawn $TERM_EMULATOR --title="gtd-capture(no-float)" -e zsh -c "gtd-capture"
hc keybind $Mod-Shift-d spawn $TERM_EMULATOR --title="gtd-today(no-float)" -e zsh -c "gtd-today"
hc keybind $Mod-b spawn xcalib -i -a

# notifications
hc keybind $Mod-q spawn dunstctl close-all

# clipboard
hc keybind $Mod-x spawn zsh -c '$(CM_LAUNCHER="rofi" clipmenu)'
# hc keybind $Mod-b spawn zsh -c 'khard email | rofi -dmenu | sed "s/^.*\s\(\S\+\)\s*$/\1/" | xsel --clipboard'

# emojis
hc keybind $Mod-0 spawn rofimoji

# pomodoro
hc keybind $Mod-a spawn zsh -c "pomodoro start"
hc keybind $Mod-Shift-a spawn zsh -c "pomodoro sb"
hc keybind $Mod-Shift-Control-a spawn zsh -c "pomodoro lb"
hc keybind $Mod-s spawn zsh -c "pomodoro-cli quit"

# passman
hc keybind $Mod-y spawn zsh -c "kpdb"
hc keybind $Mod-u spawn zsh -c "passman clip -w"
hc keybind $Mod-Shift-u spawn zsh -c "passman clip --username --window"

# Print screen
hc keybind Print spawn gscreenshot-cli -c -f ~/pictures/screenshots
hc keybind Shift-Print spawn gscreenshot-cli -s -c -f ~/pictures/screenshots

# Volume
# hc keybind XF86AudioMute spawn amixer -D pulse set Master toggle
# hc keybind XF86AudioLowerVolume spawn amixer -D pulse set Master 5%-
# hc keybind XF86AudioRaiseVolume spawn amixer -D pulse set Master 5%+

# Spotify
hc keybind $Mod-Left        spawn spt pb --previous
hc keybind $Mod-Right       spawn spt pb --next
hc keybind $Mod-Up          spawn spt pb --shuffle
hc keybind $Mod-Down        spawn spt pb --toggle
hc keybind $Mod-Shift-Left  spawn playerctl --all-players previous
hc keybind $Mod-Shift-Right spawn playerctl --all-players next
hc keybind $Mod-Shift-Up    spawn playerctl --all-players play
hc keybind $Mod-Shift-Down  spawn playerctl --all-players pause

# basic movement
# focusing clients
# hc keybind $Mod-Left  focus left
# hc keybind $Mod-Down  focus down
# hc keybind $Mod-Up    focus up
# hc keybind $Mod-Right focus right
hc keybind $Mod-h     focus left
hc keybind $Mod-j     focus down
hc keybind $Mod-k     focus up
hc keybind $Mod-l     focus right

# moving clients
# hc keybind $Mod-Shift-Left  shift left
# hc keybind $Mod-Shift-Down  shift down
# hc keybind $Mod-Shift-Up    shift up
# hc keybind $Mod-Shift-Right shift right
hc keybind $Mod-Shift-h     shift left
hc keybind $Mod-Shift-j     shift down
hc keybind $Mod-Shift-k     shift up
hc keybind $Mod-Shift-l     shift right

hc keybind $Mod-Control-Shift-d attr clients.focus.decorated toggle
hc keybind $Mod-Shift-m set_attr clients.focus.minimized true
hc keybind $Mod-Control-m jumpto last-minimized

# splitting frames
# create an empty frame at the specified direction
hc keybind $Mod-Control-Shift-v       split   right  0.5
hc keybind $Mod-Control-Shift-s       split   bottom 0.5
# let the current frame explode into subframes
hc keybind $Mod-z split explode
hc keybind $Mod-Control-Shift-m spawn zsh -c "autorandr -c"

# Virtual monitors (TODO get numbers programmatically)
hc keybind $Mod-Control-Shift-1 set_monitors $($hc_virt_monitors 1 1)
hc keybind $Mod-Control-Shift-2 set_monitors $($hc_virt_monitors 2 1)
hc keybind $Mod-Control-Shift-3 chain , set_monitors $(hc_virt_monitors 1 2) , pad 1 14 0 0 0
hc keybind $Mod-Control-Shift-4 chain , set_monitors $(hc_virt_monitors 2 2) , pad 1 14 0 0 0

# resizing frames
resizestep=0.05
hc keybind $Mod-Control-Shift-h       resize left +$resizestep
hc keybind $Mod-Control-Shift-j       resize down +$resizestep
hc keybind $Mod-Control-Shift-k       resize up +$resizestep
hc keybind $Mod-Control-Shift-l       resize right +$resizestep
hc keybind $Mod-Control-Shift-Left    resize left +$resizestep
hc keybind $Mod-Control-Shift-Down    resize down +$resizestep
hc keybind $Mod-Control-Shift-Up      resize up +$resizestep
hc keybind $Mod-Control-Shift-Right   resize right +$resizestep

# tags
tag_names=( {1..9} )
tag_keys=( {1..9} 0 )

hc rename default "${tag_names[0]}" || true
for i in ${!tag_names[@]} ; do
    hc add "${tag_names[$i]}"
    key="${tag_keys[$i]}"
    if ! [ -z "$key" ] ; then
        hc keybind "$Mod-$key" use_index "$i"
        hc keybind "$Mod-Shift-$key" move_index "$i"
    fi
done

# cycle through tags
# hc keybind $Mod-period use_index +1 --skip-visible
hc keybind $Mod-period use_index +1
hc keybind $Mod-comma  use_index -1

# layouting
hc keybind $Mod-r remove
hc keybind $Mod-Shift-f floating toggle
hc keybind $Mod-f fullscreen toggle
hc keybind $Mod-Control-f decorated toggle  # TODO do we need set_attr?
hc keybind $Mod-p pseudotile toggle
# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
# I.e. if there are two windows within a frame, the grid layout is skipped.
hc keybind $Mod-slash                                                           \
            or , and . compare tags.focus.curframe_wcount = 2                   \
                     . cycle_layout +1 vertical horizontal max vertical grid    \
               , cycle_layout +1

# mouse
hc mouseunbind --all
hc mousebind $Mod-Button1 move
hc mousebind $Mod-Button2 zoom
hc mousebind $Mod-Button3 resize

# focus
hc keybind $Mod-BackSpace      cycle_monitor
hc keybind $Mod-Control-Tab       cycle_all +1
hc keybind $Mod-Control-Shift-Tab cycle_all -1
hc keybind $Mod-Tab            cycle +1
hc keybind $Mod-Shift-Tab      cycle -1
hc keybind $Mod-i jumpto urgent

# theme
source ~/.config/colors.sh
hc attr theme.tiling.reset 1
hc attr theme.floating.reset 1
hc set frame_border_active_color '#222222'
hc set frame_border_normal_color '#101010'
hc set frame_bg_normal_color '#565656'
hc set frame_bg_active_color $ORANGE_LIGHT
hc set frame_border_width 1
hc set always_show_frame 1
hc set frame_bg_transparent 1
hc set frame_transparent_width 5
hc set frame_gap 4

hc attr theme.active.color $ORANGE_DARK
hc attr theme.normal.color '#454545'
hc attr theme.urgent.color orange
hc attr theme.inner_width 1
hc attr theme.inner_color black
hc attr theme.border_width 3
hc attr theme.floating.border_width 4
hc attr theme.floating.outer_width 1
hc attr theme.floating.outer_color black
hc attr theme.active.inner_color '#3E4A00'
hc attr theme.active.outer_color '#3E4A00'
hc attr theme.background_color '#141414'

hc set window_gap 0
hc set frame_padding 0
hc set smart_window_surroundings 0
hc set smart_frame_surroundings 1
hc set mouse_recenter_gap 0
hc set hide_covered_windows 1

# rules
hc unrule -F
#hc rule class=XTerm tag=3 # move all xterms to tag 3
hc rule focus=on # normally focus new clients
#hc rule focus=off # normally do not focus new clients
# give focus to most common terminals
#hc rule class~'(.*[Rr]xvt.*|.*[Tt]erm|Konsole)' focus=on
hc rule fixedsize floating=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' floating=on
hc rule windowtype='_NET_WM_WINDOW_TYPE_DIALOG' focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)' manage=off

# Application rules
hc rule class=qutebrowser tag=2 focus=on switchtag=on
hc rule class=firefox tag=2 focus=on switchtag=on
hc rule title=fzfmenu floating=on floatplacement=center
hc rule title~'.*\(no-float\).*' floating=on floatplacement=center
hc rule title=florence floating=on
hc rule title=Faerg floating=on floatplacement=center
hc rule title~[Un]board focus=off
hc rule class=Slack tag=3
hc rule class=zoom tag=4
hc rule class=KeePassXC title~'.*kdbx.*' tag=5 focus=on switchtag=on
hc rule class=zoom title=zoom pseudotile=on
hc rule class=floating floating=on floatplacement=center

hc set tree_style '╾│ ├└╼─┐'

# unlock, just to be sure
hc unlock

# do multi monitor setup here, e.g.:
# hc set_monitors 1280x1024+0+0 1280x1024+1280+0
# or simply:
hc detect_monitors
hc set auto_detect_monitors true

# find the panel
panel=~/.config/herbstluftwm/panel.sh
[ -x "$panel" ] || panel=/etc/xdg/herbstluftwm/panel.sh
for monitor in $(herbstclient list_monitors | cut -d: -f1) ; do
    # start it on each monitor
    "$panel" $monitor &
done
